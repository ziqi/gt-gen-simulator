/*******************************************************************************
 * Copyright (c) 2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Plugins/tpm_example/tpm_example.h"

#include <gtest/gtest.h>
#include <osi_common.pb.h>
#include <osi_trafficupdate.pb.h>

namespace gtgen::plugins
{

class TpmExampleTest : public testing::Test
{
  public:
    TpmExampleTest()
    {
        SetHostVehicleId(1);
        auto* new_moving_object = GetMutableGroundTruth().mutable_moving_object()->Add();

        new_moving_object = GetMutableGroundTruth().mutable_moving_object()->Add();
        new_moving_object->mutable_id()->set_value(2);
        new_moving_object->mutable_base()->mutable_position()->set_x(110.0);

        new_moving_object->mutable_id()->set_value(1);
        new_moving_object->mutable_base()->mutable_position()->set_x(100.0);
    }

    const osi3::GroundTruth& GetGroundTruth() const { return sensor_view_.global_ground_truth(); }

    osi3::GroundTruth& GetMutableGroundTruth() { return *sensor_view_.mutable_global_ground_truth(); }

    const osi3::SensorView& GetSensorView() const { return sensor_view_; }

    void SetHostVehicleId(std::uint64_t id) { sensor_view_.mutable_host_vehicle_id()->set_value(id); }

  private:
    osi3::SensorView sensor_view_;
};

TEST_F(TpmExampleTest, GivenTpm_WhenUpdate_ThenHasCorrectPosition)
{
    TpmExample tpm_example{};

    auto traffic_update_result =
        tpm_example.Update(std::chrono::milliseconds(100), GetSensorView(), osi3::TrafficCommand{});

    EXPECT_DOUBLE_EQ(traffic_update_result.traffic_update.update(0).base().position().x(), 100.8333);
    EXPECT_DOUBLE_EQ(traffic_update_result.traffic_update.update(0).base().velocity().x(), 8.333);
    EXPECT_EQ(traffic_update_result.traffic_update.internal_state(0).host_vehicle_id().value(), 1);
}

}  // namespace gtgen::plugins
