/*******************************************************************************
 * Copyright (c) 2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Plugins/tpm_example/tpm_example.h"

#include <google/protobuf/stubs/common.h>

namespace gtgen::plugins
{

namespace detail
{

class StaticDeinit
{
  public:
    ~StaticDeinit() { google::protobuf::ShutdownProtobufLibrary(); }
};

const StaticDeinit kStaticDeinit;

osi3::MovingObject GetHostVehicle(const osi3::SensorView& sensor_view)
{
    const auto host_vehicle_id = sensor_view.host_vehicle_id().value();

    for (const auto& moving_object : sensor_view.global_ground_truth().moving_object())
    {
        if (moving_object.id().value() == host_vehicle_id)
        {
            return moving_object;
        }
    }
    return {};
}

}  // namespace detail

using osi_traffic_participant::ITrafficParticipantModel;

TpmExample::~TpmExample() = default;

std::shared_ptr<ITrafficParticipantModel> TpmExample::Create(
    [[maybe_unused]] uint64_t entity_id,
    [[maybe_unused]] const osi3::GroundTruth& ground_truth_init,
    [[maybe_unused]] const std::map<std::string, std::string>& parameters)
{
    return std::make_shared<TpmExample>();
}

std::optional<osi3::SensorViewConfiguration> TpmExample::GetSensorViewConfigurationRequest() const
{
    return std::nullopt;
}

void TpmExample::SetSensorViewConfiguration([[maybe_unused]] osi3::SensorViewConfiguration sensor_view_config)
{
    return;
}

TpmExample::TpmExample()
{
    std::cout << "Creating a Dummy ALKS TPM Controller. " << std::endl;
}

osi_traffic_participant::TrafficParticipantUpdateResult TpmExample::Update(
    std::chrono::milliseconds delta_time,
    const osi3::SensorView& sensor_view,
    [[maybe_unused]] const std::optional<osi3::TrafficCommand>& traffic_command)
{
    assert(sensor_view.global_ground_truth().moving_object_size() > 0 &&
           "TpmExample: received groundtruth does not contain any moving objects.");

    osi3::TrafficUpdate traffic_update;
    auto* internal_state = traffic_update.add_internal_state();
    internal_state->mutable_host_vehicle_id()->CopyFrom(sensor_view.host_vehicle_id());

    auto* update_object = traffic_update.add_update();
    update_object->CopyFrom(detail::GetHostVehicle(sensor_view));

    auto* base = update_object->mutable_base();
    const auto velocity_mps = 8.333;  // 30 km/h
    const auto move_distance = velocity_mps * static_cast<double>(delta_time.count()) / 1000;
    base->mutable_velocity()->set_x(velocity_mps);
    base->mutable_position()->set_x(base->position().x() + move_distance);

    return {traffic_update, osi3::TrafficCommandUpdate{}};
}

}  // namespace gtgen::plugins
