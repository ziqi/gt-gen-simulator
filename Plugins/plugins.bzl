def _impl(ctx):
    """Create a symlink to a plugin library"""

    ctx.actions.symlink(
        output = ctx.outputs.dest,
        target_file = ctx.files.src[0],
    )

symlink_binary = rule(
    implementation = _impl,
    attrs = {
        "dest": attr.output(mandatory = True),
        "src": attr.label(mandatory = True, allow_single_file = True),
    },
)
