/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_CLI_SLEEPTIMER_H
#define GTGEN_SIMULATOR_CLI_SLEEPTIMER_H

#include <chrono>

namespace gtgen::simulator::cli
{

class SleepTimer
{
  public:
    SleepTimer(std::chrono::milliseconds step_size, double time_scale);

    std::chrono::milliseconds ComputeSleepTime(const std::chrono::time_point<std::chrono::steady_clock>& before,
                                               const std::chrono::time_point<std::chrono::steady_clock>& after) const;

  private:
    std::chrono::milliseconds step_size_ms_{0};
    double time_scale_{1.0};
};

}  // namespace gtgen::simulator::cli

#endif  // GTGEN_SIMULATOR_CLI_SLEEPTIMER_H
