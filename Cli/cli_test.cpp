/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Cli/cli.h"

#include "Cli/command_line_parser.h"
#include "Cli/version.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Version/version.h"
#include "Simulator/Export/version.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <array>

namespace gtgen::simulator::cli
{

namespace fs = gtgen::core::fs;

class MockCli : public Cli
{
  public:
    MockCli() = delete;

    explicit MockCli(const CommandLineParser& command_line_parser) : Cli(command_line_parser) {}

    std::uint16_t GetSimulationParameterStepSize() const { return simulation_parameters_.step_size_ms; }

    MOCK_METHOD(void, InstallUserData, (), (const, override));
};

class CommandLineParserBuilder
{
  public:
    void AddParameter(const char* parameter) { argv_.push_back(parameter); }

    const CommandLineParser& Build()
    {
        cmd_line_parser_.Parse(static_cast<int>(argv_.size()), &argv_[0]);
        return cmd_line_parser_;
    }

  private:
    std::vector<const char*> argv_{};
    CommandLineParser cmd_line_parser_{};
};

TEST(CliTest, GivenCli_WhenHelpAddedAsCommandLineParameter_ThenHelpIsPrinted)
{
    testing::internal::CaptureStdout();

    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-h");

    Cli cli{cmd_builder.Build()};

    EXPECT_EQ(cli.Execute(), 0);

    std::string std_out = testing::internal::GetCapturedStdout();
    EXPECT_THAT(std_out, testing::HasSubstr("usage:"));
}

TEST(CliTest, GivenCli_WhenVersionAddedAsCommandLineParameter_ThenVersionIsPrinted)
{
    testing::internal::CaptureStdout();

    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-v");

    Cli cli{cmd_builder.Build()};

    EXPECT_EQ(cli.Execute(), 0);

    std::string std_out = testing::internal::GetCapturedStdout();
    EXPECT_THAT(std_out, testing::HasSubstr(fmt::format("Cli: {}", gtgen_cli_version)));
    EXPECT_THAT(std_out, testing::Not(testing::HasSubstr("Cli: 9999.9999.9999")));

    EXPECT_THAT(std_out, testing::HasSubstr(fmt::format("GtGen-Core: {}", core::GetGtGenCoreVersion())));
    EXPECT_THAT(std_out, testing::Not(testing::HasSubstr("GtGen-Core: 9999.9999.9999")));

    EXPECT_THAT(std_out, testing::HasSubstr(fmt::format("GtGen-Simulator: {}", GetSimulatorVersion())));
    EXPECT_THAT(std_out, testing::Not(testing::HasSubstr("GtGen-Simulator: 9999.9999.9999")));
}

TEST(CliTest, GivenInitializedCli_WhenScenarioExecutionIsRequested_ThenRunSimulationIsExecuted)
{
    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-d");
    cmd_builder.AddParameter("./Simulator/Tests/Data");
    cmd_builder.AddParameter("-s");
    cmd_builder.AddParameter("stationery_ego.xosc");

    Cli cli{cmd_builder.Build()};

    EXPECT_EQ(cli.RunSimulation(), Cli::RunCode::kSimulationTimeOver);
}

TEST(CliTest, GivenInitializedCli_WhenValidateMapIsRequested_ThenValidateMapIsExecuted)
{
    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-d");
    cmd_builder.AddParameter("./Simulator/Tests/Data");
    cmd_builder.AddParameter("-m");
    cmd_builder.AddParameter("simple_road.xodr");

    Cli cli{cmd_builder.Build()};

    EXPECT_LE(cli.Execute(), 1);
}

TEST(CliTest, DISABLED_GivenInitializedCli_WhenValidateNonexistentMapIsRequested_ThenMapIsInvalid)
{
    // DISABLED because the RLS does not validate the map yet
    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-d");
    cmd_builder.AddParameter("./Simulator/Tests/Data");
    cmd_builder.AddParameter("-m");
    cmd_builder.AddParameter("simple_road-nonexistent.xodr");

    Cli cli{cmd_builder.Build()};

    EXPECT_ANY_THROW(cli.Execute());
}

TEST(CliTest, GivenInitializedCli_WhenInstallUserDataIsRequested_ThenInstallUserDataIsExecuted)
{
    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-i");
    cmd_builder.AddParameter("-d");
    fs::path temp_test_path = fs::current_path() / "tmp";
    cmd_builder.AddParameter(temp_test_path.c_str());

    MockCli mock_cli{cmd_builder.Build()};

    EXPECT_CALL(mock_cli, InstallUserData).Times(1);
    mock_cli.Execute();
}

TEST(CliTest, GivenInitializedCli_WhenValidateScenarioIsRequested_ThenValidateScenarioIsExecuted)
{
    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-j");
    cmd_builder.AddParameter("-d");
    cmd_builder.AddParameter("./Simulator/Tests/Data");
    cmd_builder.AddParameter("-s");
    cmd_builder.AddParameter("stationery_ego.xosc");

    Cli cli{cmd_builder.Build()};

    EXPECT_EQ(cli.Execute(), 0);
}

TEST(CliTest, GivenInitializedCli_WhenValidateOpenScenarioIsRequested_ThenValidateScenarioIsExecuted)
{
    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-j");
    cmd_builder.AddParameter("-d");
    cmd_builder.AddParameter("./Simulator/Tests/Data");
    cmd_builder.AddParameter("-s");
    cmd_builder.AddParameter("stationery_ego.xosc");

    Cli cli{cmd_builder.Build()};

    EXPECT_EQ(cli.Execute(), 0);
}

TEST(CliTest, GivenInitializedCli_WhenValidateScenarioWithoutScenarioIsRequested_ThenScenarioIsNotValidated)
{
    CommandLineParserBuilder cmd_builder{};
    cmd_builder.AddParameter("my_gtgen_cli");
    cmd_builder.AddParameter("-j");

    Cli cli{cmd_builder.Build()};

    EXPECT_ANY_THROW(cli.Execute());
}

TEST(CliTest, GivenInitializedCli_WhenStepSizeIsProvidedViaCommandLine_ThenStepSizeIsSet)
{
    std::uint16_t expected_step_size_ms{42};

    std::string s1{"gtgen_cli"};
    std::string s2{"--scenario"};
    std::string s3{"stationery_ego.xosc"};
    std::string s4{"--gtgen-data"};
    std::string s5{"./Simulator/Tests/Data"};
    std::string s6{"--step-size-ms"};
    std::string s7 = std::to_string(expected_step_size_ms);
    std::array<const char*, 7> argv = {s1.data(), s2.data(), s3.data(), s4.data(), s5.data(), s6.data(), s7.data()};

    CommandLineParser cmd_line_parser{};
    cmd_line_parser.Parse(argv.size(), argv.data());

    MockCli cli{cmd_line_parser};

    EXPECT_EQ(cli.RunSimulation(), Cli::RunCode::kSimulationTimeOver);
    EXPECT_EQ(expected_step_size_ms, cli.GetSimulationParameterStepSize());
}

TEST(CliTest, GivenInitializedCli_WhenStepSizeIsNotProvidedViaCommandLine_ThenDefaultStepSizeIsUsed)
{
    std::uint16_t expected_default_step_size{10};
    std::string s1{"gtgen_cli"};
    std::string s2{"--scenario"};
    std::string s3{"stationery_ego.xosc"};
    std::string s4{"--gtgen-data"};
    std::string s5{"./Simulator/Tests/Data"};
    std::array<const char*, 5> argv = {s1.data(), s2.data(), s3.data(), s4.data(), s5.data()};

    CommandLineParser cmd_line_parser{};
    cmd_line_parser.Parse(argv.size(), argv.data());

    MockCli cli{cmd_line_parser};

    EXPECT_EQ(cli.RunSimulation(), Cli::RunCode::kSimulationTimeOver);
    EXPECT_EQ(expected_default_step_size, cli.GetSimulationParameterStepSize());
}

}  // namespace gtgen::simulator::cli
