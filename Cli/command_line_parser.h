/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_CLI_COMMANDLINEPARSER_H
#define GTGEN_SIMULATOR_CLI_COMMANDLINEPARSER_H

#include "Core/Export/simulation_parameters.h"

#include <string>

namespace gtgen::simulator::cli
{
/// @brief This class is in charge of parsing the command line arguments. It supports to print a help and the current
/// GtGen core version. The command line parameters are provided as SimulationParameters.
/// Note: This class is used before the loggers are setup
class CommandLineParser final
{
  public:
    /// @brief Takes the command line arguments and parses them. Returns false on parse error, true otherwise
    bool Parse(int argc, const char** argv);

    /// @brief Returns information provided by parser (e.g. error or help) if applicable
    std::string GetParserMessage() const { return parser_message_; }

    /// @brief Returns the command line parameters in a struct
    const gtgen::core::SimulationParameters& GetSimulationParameters() const { return simulation_parameters_; }

    /// @brief Whether the ros groundtruth visualizer server should be launched
    bool ShouldValidateScenario() const { return validate_scenario_; }
    bool ShouldInstallUserData() const { return install_user_data_; }

  private:
    gtgen::core::SimulationParameters simulation_parameters_{};
    std::string parser_message_{};
    bool validate_scenario_{false};
    bool install_user_data_{false};
};

}  // namespace gtgen::simulator::cli

#endif  // GTGEN_SIMULATOR_CLI_COMMANDLINEPARSER_H
