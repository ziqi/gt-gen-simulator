/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Cli/command_line_parser.h"

#include "Cli/version.h"
#include "Core/Service/Version/version.h"
#include "Simulator/Export/version.h"

#include <clara/clara.hpp>

namespace gtgen::simulator::cli
{
bool CommandLineParser::Parse(int argc, const char** argv)
{
    // Default log level to 'Info' in standalone gt-gen-simulator
    simulation_parameters_.console_log_level = "Info";

    bool show_help{false};
    bool print_version{false};

    auto cli =
        clara::Help(show_help) |
        clara::Opt(print_version)["-v"]["--version"]("Prints application version information") |
        clara::Opt(simulation_parameters_.scenario,
                   "Scenario")["-s"]["--scenario"]("Mandatory argument to specify the scenario to execute") |
        clara::Opt(simulation_parameters_.console_log_level, "Info")["-c"]["--console-log-level"](
            "Specifies the console log level. Default is 'Info'. Supported log levels are: 'Trace', 'Debug', 'Info', "
            "'Warn', 'Error' and 'Off'") |
        clara::Opt(simulation_parameters_.custom_log_directory, "LogFileDirectory")["-l"]["--log-file-dir"](
            "If this argument is not given, file logging will be done to GTGEN_DATA/log. To define a folder in the "
            "home folder pass in an argument starting with ~, or a full path starting with /. If neither a ~ nor a / "
            "is given at the beginning, the logfile folder will be created relative to the executable") |
        clara::Opt(simulation_parameters_.custom_user_settings_path, "UserSettingsFile")["-u"]["--user-settings-file"](
            "Uses the given config file instead of the default UserSettings.ini") |
        clara::Opt(simulation_parameters_.map_path, "MapPath")["-m"]["--validate-map"](
            "Runs validation for the provided (OpenDrive) map. Has higher precedence than the scenario parameter.") |
        clara::Opt(simulation_parameters_.custom_gtgen_data_directory,
                   "GtGenDirectory")["-d"]["--gtgen-data"]("Sets the GTGEN_DATA directory.") |
        clara::Opt(simulation_parameters_.step_size_ms,
                   "StepSizeMillis")["-t"]["--step-size-ms"]("Sets the step size in milliseconds.") |
        clara::Opt(install_user_data_)["-i"]["--install"](
            "Installs GTGEN_DATA into home directory, if no custom directory is specified (with -d option).") |
        clara::Opt(validate_scenario_)["-j"]["--validate-scenario"](
            "Runs validation of the scenario provided with -s/--scenario. Doesn't start the simulation.");

    auto result = cli.parse(clara::Args{argc, argv});
    if (!result)
    {
        std::stringstream ss;
        ss << "Error while parsing command line parameters: " << result.errorMessage() << "\n";
        for (int i = 0; i < argc; i++)
        {
            ss << " " << argv[i];  // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        }
        ss << (clara::Parser() | cli);
        parser_message_ = ss.str();
        return false;
    }

    if (show_help)
    {
        std::stringstream ss;
        ss << (clara::Parser() | cli);
        parser_message_ = ss.str();
    }
    else if (print_version)
    {
        parser_message_ = fmt::format("Cli: {}\nGtGen-Core: {}\nGtGen-Simulator: {}",
                                      gtgen_cli_version,
                                      core::GetGtGenCoreVersion(),
                                      GetSimulatorVersion());
    }
    else if (install_user_data_ || !simulation_parameters_.map_path.empty())
    {
        // execute installation, map validation or launch ros gt visualizer
        return true;
    }
    else if (simulation_parameters_.scenario.empty())
    {
        parser_message_ =
            "Missing mandatory scenario file. Please specify the:\n"
            " - scenario file with [-s|--scenario] path/to/scenario.xosc";
        return false;
    }

    return true;
}

}  // namespace gtgen::simulator::cli
