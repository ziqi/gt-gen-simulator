/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Cli/sleep_timer.h"

#include <gtest/gtest.h>

namespace gtgen::simulator::cli
{

using tp_type = std::chrono::steady_clock::time_point;

std::pair<tp_type, tp_type> GetTestTimePoints()
{
    std::tm tm = {};
    std::istringstream in{
        "21/12/2012 16:31:32.0000\n"
        "21/12/2012 16:31:32.0010"};

    std::chrono::steady_clock::time_point tp1;
    in >> std::get_time(&tm, "%b %d %Y %H:%M:%S");

    std::chrono::steady_clock::time_point tp2;
    in >> std::get_time(&tm, "%b %d %Y %H:%M:%S");

    return {tp1, tp2};
}

class SleepTimerTest : public testing::TestWithParam<double>
{
};

TEST_P(SleepTimerTest, GivenStepSizeAndNormalTimeScale_WhenComputeSleepTime_ThenSleepTimeComputedCorrectly)
{
    const double time_scale{GetParam()};
    const std::chrono::milliseconds step_size{100};
    const auto [before, after] = GetTestTimePoints();
    const auto fake_run_time = std::chrono::duration_cast<std::chrono::milliseconds>(after - before);  // 10 ms
    const auto expected_sleep_time =
        std::chrono::duration_cast<std::chrono::milliseconds>(step_size * (1 / time_scale)) - fake_run_time;

    SleepTimer sleep_timer{step_size, time_scale};

    EXPECT_EQ(sleep_timer.ComputeSleepTime(before, after), expected_sleep_time);
}

INSTANTIATE_TEST_SUITE_P(GivenStepSizeAndNormalTimeScale_WhenComputeSleepTime_ThenSleepTimeComputedCorrectly,
                         SleepTimerTest,
                         testing::Values(1.0, 0.5, 0.1, 1.5, 10.0));

}  // namespace gtgen::simulator::cli
