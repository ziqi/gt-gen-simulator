/*******************************************************************************
 * Copyright (c) 2020-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Cli/cli.h"

#include "Cli/command_line_parser.h"
#include "Cli/sleep_timer.h"
#include "Core/Environment/GtGenEnvironment/environment_factory.h"
#include "Core/Service/Profiling/profiling.h"
#include "Simulator/Export/simulation_controller.h"
#include "Simulator/Export/simulator.h"
#include "Simulator/ScenarioEngines/ScenarioEngineFactory/scenario_engine_factory.h"

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Execution/i_scenario_engine.h>

#include <atomic>
#include <iostream>
#include <memory>
#include <thread>

namespace gtgen::simulator::cli
{

Cli::Cli(const CommandLineParser& cmd_line_parser)
    : cmd_line_parser_{cmd_line_parser}, simulation_parameters_{cmd_line_parser_.GetSimulationParameters()}
{
    user_data_manager_ = std::make_unique<simulation::user_data::UserDataManager>(
        simulation_parameters_.custom_gtgen_data_directory, simulation_parameters_.custom_user_settings_path);
}

int Cli::Execute()
{
    // order is important!

    if (!simulation_parameters_.map_path.empty())
    {
        return ValidateMap();
    }

    if (cmd_line_parser_.ShouldInstallUserData())
    {
        InstallUserData();
        return 0;
    }

    if (cmd_line_parser_.ShouldValidateScenario())
    {
        return ValidateScenario();
    }

    const auto parser_message = cmd_line_parser_.GetParserMessage();
    if (!parser_message.empty())
    {
        // Logging framework not yet setup
        std::cout << parser_message << std::endl;
        return 0;
    }

    auto exit_code = RunSimulation();
    if (exit_code == Cli::RunCode::kSimulationTimeOver)
    {
        return 0;
    }
    return 1;
}

void Cli::InstallUserData() const
{
    user_data_manager_->Install();
}

int Cli::ValidateMap() const
{
    // TODO: need to call RLS validation when fixed.
    return 0;
}

int Cli::ValidateScenario() const
{
    return gtgen::simulator::Simulator::ValidateScenario(simulation_parameters_);
}

Cli::RunCode Cli::RunSimulation()
{
    std::atomic_bool run_simulation{true};

    SimulationController sim_controller{simulation_parameters_.console_log_level};
    simulation_parameters_.control_server_port = sim_controller.GetServerPort();

    auto simulator = std::make_unique<Simulator>(simulation_parameters_);
    simulator->Init();

    sim_controller.SetSimulationStepCallback([&simulator] { simulator->Step(); });
    sim_controller.SetSimulationAbortCallback([&run_simulation] { run_simulation = false; });

    SleepTimer sleep_timer{simulator->GetStepSize(), simulator->GetTimeScale()};

    while (!simulator->IsFinished())
    {
        GTGEN_PROFILE_NEW_FRAME

        GTGEN_PROFILE_SCOPE

        auto time_point_before_run_once = std::chrono::steady_clock::now();
        sim_controller.ExecuteStep();
        auto time_point_after_run_once = std::chrono::steady_clock::now();

        if (!run_simulation)
        {
            return RunCode::kSimulationTerminated;
        }

        {
            GTGEN_PROFILE_SCOPE
            auto sleep_time = sleep_timer.ComputeSleepTime(time_point_before_run_once, time_point_after_run_once);
            std::this_thread::sleep_for(sleep_time);
        }
    }

    return RunCode::kSimulationTimeOver;
}

}  // namespace gtgen::simulator::cli
