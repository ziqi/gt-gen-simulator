#!/bin/bash

# *******************************************************************************
# Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
# Copyright (C) 2024, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

set -o errexit  # abort script when a command has a non-zero exit status
set -o nounset  # abort script when accessing an unbound variable
set -o pipefail # non-zero exit statuses within a pipe are not hidden
set -o errtrace # allow trapping errors that happen in functions and subshells

# Function to extract version components from version.bzl

# shellcheck disable=SC1091
source "$(dirname "${BASH_SOURCE[0]}")/../../helper_functions/bash_helper.sh"

FAILED=0            # number of FAILED checks
PERFORM_DRY_RUN="1" # no fix is applied, only check
VERSION_FILE_PATH="Simulator/LibSimCore/version.bzl"

# Extract version components from version.bzl
function extract_version_from_file()
{
    local file=$1
    if [[ -f "${file}" ]]; then
        local major minor patch
        major=$(grep '_MAJOR\s' "${file}" | awk -F'"' '{print $2}')
        minor=$(grep '_MINOR\s' "${file}" | awk -F'"' '{print $2}')
        patch=$(grep '_PATCH\s' "${file}" | awk -F'"' '{print $2}')
        echo "${major}.${minor}.${patch}"
    else
        log::sub_failure "Error: File ${file} not found."
        ((FAILED += 1))
    fi

}

function check_version()
{
    # Get the latest tag of the current commit
    local tags
    tags=$(git tag --points-at HEAD)

    # Check if tags is empty
    if [[ -z "${tags}" ]]; then
        log::warn "No tags found, skipping version check."
        return
    fi

    # Iterate over each tag and check if it matches the version pattern
    local tag
    for tag in ${tags}; do
        if [[ "${tag}" =~ ^v([0-9]+)\.([0-9]+)\.([0-9]+)$ ]]; then
            local tag_version="${BASH_REMATCH[1]}.${BASH_REMATCH[2]}.${BASH_REMATCH[3]}"

            # Extract version from version.bzl
            local file_version
            file_version=$(extract_version_from_file "${VERSION_FILE_PATH}")

            # Compare the tag version with the file version
            if [[ "${tag_version}" != "${file_version}" ]]; then
                log::sub_failure "Error: The tag version (${tag_version}) does not match the version file (${file_version})."
                ((FAILED += 1))
            fi
        fi
    done
}

function main()
{
    local title
    title="Check Version between git tag and ${VERSION_FILE_PATH}"

    log::run "${title}"

    check_version

    log::summary "${title}" "${FAILED}" "${PERFORM_DRY_RUN}"

    return "${FAILED}"
}

main "$@"
