#!/bin/bash

# *******************************************************************************
# Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
# Copyright (C) 2024-2025, ANSYS, Inc.
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# *******************************************************************************

set -o errexit  # abort script when a command has a non-zero exit status
set -o nounset  # abort script when accessing an unbound variable
set -o pipefail # non-zero exit statuses within a pipe are not hidden
set -o errtrace # allow trapping errors that happen in functions and subshells

TOLERANCE_GB=50 # 50GB tolerance
CACHE_PATH=""

function get_folder_size_in_gb()
{
    local size_in_bytes
    if ! size_in_bytes=$(du -sb "${CACHE_PATH}" | cut -f1); then
        echo "Cannot get size of  ${CACHE_PATH}. Please retriger the CI job in a few minutes."
        exit 1
    fi

    local size_in_gb
    size_in_gb=$(awk "BEGIN {printf \"%.2f\", ${size_in_bytes} / (1024 * 1024 * 1024)}")
    echo "${size_in_gb}"
}

# Function to change permissions and retry removal
function retry_removal()
{
    local target_dir="$1"
    local log_file="/tmp/clean_cache_error.log"
    # shellcheck disable=SC2064
    trap "rm -f ${log_file}" EXIT

    while true; do
        # Attempt to remove the directory, and redirect stderr to a log file
        rm -rf "${target_dir:?}"/* 2>"${log_file}"

        # Check if there are any permission denied errors, if so, change permissions
        if grep -q "Permission denied" ${log_file}; then
            grep "Permission denied" ${log_file} | awk -F"'" '{print $2}' | xargs chmod u+w
        else
            break
        fi
    done
}

function main()
{
    # Check if a folder argument is provided
    if [[ -z "${1-}" ]]; then
        echo "Usage: $0 /path/to/folder"
        exit 1
    fi

    CACHE_PATH="$1"
    readonly CACHE_PATH
    local size_in_gb
    size_in_gb=$(get_folder_size_in_gb "${CACHE_PATH}")
    echo "Folder (${CACHE_PATH}) size: ${size_in_gb} GB"

    if awk "BEGIN {exit !(${size_in_gb} > ${TOLERANCE_GB})}"; then
        echo "Cache folder ${CACHE_PATH} size exceeds ${TOLERANCE_GB} GB. Clearing cache contents ..."
        echo ""
        echo -e "*******************************************************************"
        echo -e "*                   ATTENTION REQUIRED                            *"
        echo -e "*                                                                 *"
        echo -e "* The cleanup task might take a long time (>30min) to complete.   *"
        echo -e "* In case the job is killed by CI node, please re-trigger the job.*"
        echo -e "*******************************************************************"
        echo ""
        retry_removal "${CACHE_PATH}"
        echo "Folder (${CACHE_PATH}) contents cleared. Folder size is now $(get_folder_size_in_gb "${CACHE_PATH}") GB."
    else
        echo "Folder (${CACHE_PATH}) size is within the tolerance of ${TOLERANCE_GB} GB. No action taken."
    fi
}

main "$@"
