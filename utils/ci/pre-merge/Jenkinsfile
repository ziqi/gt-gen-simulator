/*******************************************************************************
 * Copyright (C) 2024, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

pipeline {
  agent none
  options {
    checkoutToSubdirectory('repo')
    timeout(time: 5, unit: 'HOURS')
    timestamps()
  }
  stages {
      stage('Linux') {
        agent {
          kubernetes {
            label 'gt-gen-simulator-agent-pod-' + env.BUILD_NUMBER
            yaml """
apiVersion: v1
kind: Pod
spec:
  securityContext:
    seLinuxOptions:
      level: s0:c4,c54
      type: spc_t
  containers:
  - name: gt-gen-simulator-build-container
    image: avxautonomy/gt-gen-dev:ubuntu20.04
    tty: true
    resources:
      limits:
        memory: "16Gi"
        cpu: "4"
      requests:
        memory: "16Gi"
        cpu: "4"
    volumeMounts:
    - name: openpass-cache-storage
      mountPath: /home/jenkins/cache
  - name: jnlp
    volumeMounts:
    - name: volume-known-hosts
      mountPath: /home/jenkins/.ssh
  volumes:
  - name: volume-known-hosts
    configMap:
      name: known-hosts
  - name: openpass-cache-storage
    persistentVolumeClaim:
      claimName: openpass-cache-storage
"""
          }
        }
      stages {
        stage('Prepare') {
          steps {
            container('gt-gen-simulator-build-container') {
              sh "bash repo/utils/ci/scripts/10_prepare.sh"
            }
          }
        }
        stage('Run style and lint checks') {
          steps {
            container('gt-gen-simulator-build-container') {
              sh "bash repo/utils/ci/scripts/20_run_style_and_lint_checks.sh"
            }
          }
        }
        stage('Run build and tests in debug') {
          steps {
            container('gt-gen-simulator-build-container') {
              sh "bash repo/utils/ci/scripts/30_build_and_checks_debug.sh"
            }
          }
        }
        stage('Generate test report') {
          steps {
            container('gt-gen-simulator-build-container') {
              sh "bash repo/utils/ci/scripts/31_generate_test_report.sh"
            }
          }
        }
        stage('Generate code coverage report') {
          steps {
            container('gt-gen-simulator-build-container') {
              sh "bash repo/utils/ci/scripts/32_generate_code_coverage.sh"
            }
          }
        }
        stage('Run Sanitizer Checks') {
          steps {
            container('gt-gen-simulator-build-container') {
              sh "bash repo/utils/ci/scripts/21_sanitizer_checks.sh"
            }
          }
        }
        stage('Run build and tests release') {
          steps {
            container('gt-gen-simulator-build-container') {
              sh "bash repo/utils/ci/scripts/40_build_and_checks_release.sh"
            }
          }
        }
        stage('Generate release artifacts') {
          steps {
            container('gt-gen-simulator-build-container') {
              sh "bash repo/utils/ci/scripts/41_create_release_package_and_test.sh"
            }
          }
        }
      }
      post {
        always {
           archiveArtifacts allowEmptyArchive: true, artifacts: 'artifacts/**', followSymlinks: false
        }
      }
    }
  }
}
