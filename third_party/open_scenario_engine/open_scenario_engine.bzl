load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v11.0.0"

def open_scenario_engine():
    maybe(
        http_archive,
        name = "open_scenario_engine",
        url = "https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/archive/{tag}/openscenario1_engine-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "c78a39831044c050a43260f6c22752b83d7428f1de612f758d7e0cdcc2fa2fd1",
        strip_prefix = "openscenario1_engine-{tag}/engine".format(tag = _TAG),
        type = "tar.gz",
        patches = [
            "@//third_party/open_scenario_engine:0001-fix-Parse-correct-event-name-for-EventNode.patch",
        ],
        patch_args = ["-p1"],
    )
