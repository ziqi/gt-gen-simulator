def create_alias_rules(modules):

    for module in modules:
        native.alias(
            name = module,
            actual = "@gt_gen_core//third_party/boost:{}".format(module),
            visibility = ["//visibility:public"],
        )
