/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

#include "Core/Export/simulation_parameters.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Service/Utility/clock.h"

#include <units.h>

#include <thread>
#include <utility>

namespace gtgen::simulator::test_utils
{

namespace fs = gtgen::core::fs;
using units::literals::operator""_ms;

double GtGenTestFixture::GetMagnitude(const osi3::Vector3d& v)
{
    mantle_api::Vec3<units::length::meter_t> tmp = gtgen::core::service::gt_conversion::ToVec3Length(v);
    return tmp.Length().value();
}

void GtGenTestFixture::InitSimulator(const std::string& user_settings,
                                     const std::string& scenario_filename,
                                     TestTrafficUpdate::TestHostVehicleMovement test_vehicle_movement,
                                     std::uint16_t step_size_ms,
                                     std::string console_log_level)
{
    step_size_ms_ = step_size_ms;

    const auto data_path = fs::path(GetGtGenDataDirectory());

    gtgen::core::SimulationParameters params{};
    params.custom_gtgen_data_directory = data_path;
    params.custom_user_settings_path = data_path / "UserSettings" / user_settings;
    params.scenario = scenario_filename;
    params.console_log_level = std::move(console_log_level);
    params.step_size_ms = step_size_ms;

    simulator_ = test_utils::CreateSimulator(params);
    simulator_->Init();

    test_traffic_update_ = std::make_unique<TestTrafficUpdate>(test_vehicle_movement);

    gtgen::core::service::utility::Clock::Instance().SetNow(0_ms);
}

void GtGenTestFixture::SpinOnce()
{
    if (!simulator_ || !test_traffic_update_)
    {
        std::cerr << "The simulator is not initialized" << std::endl;
    }

    simulator_->Step();
    test_traffic_update_->Step(GetLastGroundTruth());
    simulator_->SetTrafficUpdate(test_traffic_update_->GetNewTrafficUpdate());
}

void GtGenTestFixture::SpinNTimes(std::uint32_t n)
{
    for (std::uint32_t i = 0; i < n; ++i)
    {
        SpinOnce();
    }
}

void GtGenTestFixture::RunFor(std::chrono::milliseconds duration)
{
    const auto wait_end_time = GetTime() + duration;
    const auto start_time = GetTime();
    std::int64_t current_time = start_time.count();

    while (current_time < wait_end_time.count())
    {
        SpinOnce();
        current_time = GetTime().count();
    }
}

// NOLINTNEXTLINE(performance-unnecessary-value-param)
void GtGenTestFixture::RunFor(std::chrono::milliseconds duration, std::function<void(std::int64_t)> user_function)
{
    const auto wait_end_time = GetTime() + duration;
    const auto start_time = GetTime();
    std::int64_t current_time = start_time.count();

    while (current_time < wait_end_time.count())
    {
        user_function(current_time);
        SpinOnce();
        current_time = GetTime().count();
    }
}

std::chrono::milliseconds GtGenTestFixture::GetTime() const
{
    const auto gt_time = GetLastGroundTruth().timestamp();
    const auto gt_seconds_in_millis = gt_time.seconds() * 1000;
    const auto gt_nanos_in_millis = static_cast<std::int64_t>(gt_time.nanos() * 1e-6);
    return std::chrono::milliseconds(gt_seconds_in_millis + gt_nanos_in_millis);
}

void GtGenTestFixture::InitialSpin()
{
    SpinOnce();

    FindLaneIdOfObservedEntity();
}

void GtGenTestFixture::FindLaneIdOfObservedEntity()
{
    const auto& gt_moving_objects = GetLastGroundTruth().moving_object();
    if (gt_moving_objects.size() > 1)
    {
        for (const auto& object : gt_moving_objects)
        {
            if (object.id().value() != 0)
            {
                if (object.moving_object_classification().assigned_lane_id_size() > 0)
                {
                    observed_entity_start_lane_id_ = object.moving_object_classification().assigned_lane_id(0).value();
                }
            }
        }
    }
}

GtGenTestFixture::OsiId GtGenTestFixture::ObservedEntityStartLaneId() const
{
    return observed_entity_start_lane_id_;
}

void GtGenTestFixture::CheckAndFailVehicleSpawns()
{
    ASSERT_TRUE(CheckIfHostVehicleExists());
    CheckNumberOfTrafficVehicles();
}

bool GtGenTestFixture::CheckIfHostVehicleExists()
{
    auto is_host = [](const auto& object) { return object.id().value() == 0; };

    const auto& moving_objects = GetLastGroundTruth().moving_object();
    return std::any_of(moving_objects.cbegin(), moving_objects.cend(), is_host);
}

void GtGenTestFixture::CheckNumberOfTrafficVehicles()
{
    const auto traffic_vehicles_count = GetLastGroundTruth().moving_object_size() - 1;
    ASSERT_EQ(expected_total_traffic_vehicles_, traffic_vehicles_count);
}

std::optional<osi3::MovingObject> GtGenTestFixture::GetMovingObjectById(OsiId id)
{
    const auto& moving_objects = GetLastGroundTruth().moving_object();
    for (const auto& object : moving_objects)
    {
        auto object_id = object.id().value();
        if (object_id == id)
        {
            return object;
        }
    }
    return std::nullopt;
}

std::optional<osi3::MovingObject> GtGenTestFixture::GetMovingObjectById(
    const std::vector<osi3::MovingObject>& moving_objects,
    OsiId id)
{
    for (const auto& object : moving_objects)
    {
        if (object.id().value() == id)
        {
            return object;
        }
    }
    return std::nullopt;
}

std::vector<osi3::MovingObject> GtGenTestFixture::GetMovingObjectsOfType(osi3::MovingObject_Type type)
{
    std::vector<osi3::MovingObject> type_objects{};

    const auto& moving_objects = GetLastGroundTruth().moving_object();
    for (const auto& object : moving_objects)
    {
        if (object.type() == type)
        {
            type_objects.push_back(object);
        }
    }
    return type_objects;
}

std::vector<osi3::MovingObject> GtGenTestFixture::GetVehicles()
{
    return GetMovingObjectsOfType(osi3::MovingObject::TYPE_VEHICLE);
}

std::vector<osi3::MovingObject> GtGenTestFixture::GetPedestrians()
{
    return GetMovingObjectsOfType(osi3::MovingObject::TYPE_PEDESTRIAN);
}

void GtGenTestFixture::CheckAndFailTrafficVehicleMovement()
{
    const auto active_traffic_vehicle = GetMovingObjectById(active_traffic_vehicle_id_);

    ASSERT_TRUE(active_traffic_vehicle);
    {
        const OsiVector current_traffic_vehicle_position = active_traffic_vehicle.value().base().position();
        EXPECT_TRUE(CheckTrafficVehiclePosition(traffic_vehicle_start_position_, current_traffic_vehicle_position));
    }
}

bool GtGenTestFixture::CheckTrafficVehicleMoved()
{
    const auto active_traffic_vehicle = GetMovingObjectById(active_traffic_vehicle_id_);

    if (active_traffic_vehicle)
    {
        OsiVector current_traffic_vehicle_position = active_traffic_vehicle.value().base().position();

        double diff =
            GetMagnitude(core::test_utils::GetDelta(current_traffic_vehicle_position, traffic_vehicle_start_position_));
        return diff > epsilon;
    }
    std::cout << "Traffic Vehicle with id: " << active_traffic_vehicle_id_ << " was not found" << std::endl;
    return false;
}

bool GtGenTestFixture::CheckTrafficVehicleIsOnHostVehicleLane()
{
    // Check if Scenario Car switched lane:
    const auto active_traffic_vehicle = GetMovingObjectById(active_traffic_vehicle_id_);
    const auto host_vehicle = gtgen::core::test_utils::GetHostVehicle(GetLastGroundTruth());

    if (active_traffic_vehicle && host_vehicle)
    {
        const auto current_traffic_vehicle_lane_id =
            active_traffic_vehicle.value().moving_object_classification().assigned_lane_id(0).value();
        const auto current_host_vehicle_lane_id =
            host_vehicle.value().moving_object_classification().assigned_lane_id(0).value();

        return current_host_vehicle_lane_id == current_traffic_vehicle_lane_id;
    }
    std::cout << "Host Vehicle or Traffic Vehicle with id: " << active_traffic_vehicle_id_ << " was not found"
              << std::endl;
    return false;
}

bool GtGenTestFixture::CheckScenarioCarLaneId(OsiId target_lane_id)
{
    const auto active_traffic_vehicle_lane_ids =
        GetMovingObjectById(active_traffic_vehicle_id_).value().moving_object_classification().assigned_lane_id();
    for (const auto& id : active_traffic_vehicle_lane_ids)
    {
        if (id.value() == target_lane_id)
        {
            return true;
        }
    }
    std::cout << "Traffic Vehicle is not in a Position that corresponds to the lane it should be in." << std::endl;
    return false;
}

void GtGenTestFixture::WaitForDistance(double expected_offset, std::uint16_t timeout)
{
    const auto& current_ground_truth = GetLastGroundTruth();
    auto previous_position = gtgen::core::test_utils::GetHostVehicle(current_ground_truth).value().base().position();
    double distance = 0;
    std::uint16_t i = 0;

    while (distance < expected_offset)
    {
        SpinOnce();
        auto current_position = gtgen::core::test_utils::GetHostVehicle(current_ground_truth).value().base().position();
        distance += GetMagnitude(core::test_utils::GetDelta(current_position, previous_position));
        previous_position = current_position;

        ASSERT_LT(i, timeout);

        if (i % 50 == 0)
        {
            std::cout << "Distance: " << distance << std::endl;
        }
        i++;
    }
    std::cout << "Distance: " << distance << std::endl;
}

bool GtGenTestFixture::CheckTrafficVehiclePosition(const OsiVector& start_position, const OsiVector& current_position)
{
    const OsiVector diff = core::test_utils::GetDelta(start_position, current_position);

    if (std::abs(diff.y()) < epsilon || std::abs(diff.x()) > epsilon || std::abs(diff.z()) > epsilon)
    {
        std::cout << "Traffic Vehicle did not move straight forward epsilon " << epsilon << " diff.X " << diff.x()
                  << " diff.Z " << diff.z() << std::endl;
        return false;
    }
    return true;
}

void GtGenTestFixture::SetTrafficUpdate(osi3::TrafficUpdate traffic_update)
{
    test_traffic_update_->SetTrafficUpdate(std::move(traffic_update));
}

void GtGenTestFixture::CreateAndSetTrafficUpdate(const osi3::Vector3d& position, const osi3::Timestamp& timestamp)
{
    osi3::TrafficUpdate traffic_update{};
    auto* moving_object = traffic_update.add_update();
    moving_object->mutable_id()->set_value(0);
    traffic_update.mutable_timestamp()->CopyFrom(timestamp);
    moving_object->mutable_base()->mutable_position()->CopyFrom(position);
    SetTrafficUpdate(traffic_update);
}

std::uint16_t GtGenTestFixture::GetStepSizeMs() const
{
    if (!simulator_)
    {
        std::cerr << "The simulator is not initialized" << std::endl;
        return 0;
    }

    return step_size_ms_;
}

std::chrono::milliseconds GtGenTestFixture::GetScenarioDuration() const
{
    if (!simulator_)
    {
        std::cerr << "The simulator is not initialized" << std::endl;
        return std::chrono::milliseconds{0};
    }

    return simulator_->GetScenarioDuration();
}

}  // namespace gtgen::simulator::test_utils
