/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_GTGENTESTFIXTURE_GTGENTESTFIXTURE_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_GTGENTESTFIXTURE_GTGENTESTFIXTURE_H

#include "Core/Service/Logging/log_setup.h"
#include "Core/Simulation/Simulator/simulator_core_impl.h"
#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"
#include "Core/Tests/TestUtils/expect_extensions.h"
#include "Simulator/Tests/TestUtils/TestTrafficUpdate/test_traffic_update.h"
#include "Simulator/Tests/TestUtils/create_simulator.h"
#include "Simulator/Utils/Logging/logging.h"
#include "osi_groundtruth.pb.h"

#include <gtest/gtest.h>

#include <chrono>
#include <memory>

namespace gtgen::simulator::test_utils
{

/// @brief Fixture for deterministic closed loop integration tests.
/// @attention You need to ensure that blocking_communication is set to true, when testing with this fixture!
class GtGenTestFixture : public testing::Test
{
  public:
    GtGenTestFixture() = default;
    ~GtGenTestFixture() override { gtgen::core::service::logging::LogSetup::Instance().CleanupLogging(); }
    GtGenTestFixture(const GtGenTestFixture&) = delete;
    GtGenTestFixture& operator=(const GtGenTestFixture&) = delete;
    GtGenTestFixture(GtGenTestFixture&&) = delete;
    GtGenTestFixture& operator=(GtGenTestFixture&&) = delete;

  protected:
    using OsiVector = osi3::Vector3d;
    using OsiOrientation = osi3::Orientation3d;
    using OsiDimension = osi3::Dimension3d;
    using OsiId = std::uint64_t;

    static constexpr float epsilon = 0.0003F;

    /// @brief Returns GTGEN_DATA directory.
    virtual std::string GetGtGenDataDirectory() const { return "./Simulator/Tests/Data"; };

    void InitSimulator(const std::string& user_settings,
                       const std::string& scenario_filename,
                       TestTrafficUpdate::TestHostVehicleMovement test_vehicle_movement,
                       std::uint16_t step_size_ms = 10,
                       std::string console_log_level = "Info");

    void SpinNTimes(std::uint32_t n);

    /// @brief Runs the simulator for a specified duration
    void RunFor(std::chrono::milliseconds duration);

    /// @brief Runs the simulator for a specified duration while executing a user function at each step, before the
    /// simulator is stepped
    void RunFor(std::chrono::milliseconds duration, std::function<void(std::int64_t)> user_function);

    /// @brief Returns the last ground truth timestamp in milliseconds.
    std::chrono::milliseconds GetTime() const;

    std::optional<osi3::MovingObject> GetVehicleAfterNSpins(std::uint64_t vehicle_id, std::uint32_t number_of_spins)
    {
        SpinNTimes(number_of_spins);
        return GetMovingObjectById(vehicle_id);
    }

    const osi3::GroundTruth& GetLastGroundTruth() const { return simulator_->GetSensorView().global_ground_truth(); }

    const osi3::SensorView& GetLastSensorView() const { return simulator_->GetSensorView(); }

    void SetTrafficUpdate(osi3::TrafficUpdate traffic_update);
    void CreateAndSetTrafficUpdate(const osi3::Vector3d& position, const osi3::Timestamp& timestamp);

    bool HostPoseRecoveredInLastStep() const
    {
        return simulator_->GetHostVehicleInterface().HostPoseRecoveredInLastStep();
    }

    std::optional<osi3::MovingObject> GetMovingObjectById(OsiId id);

    std::optional<osi3::MovingObject> GetMovingObjectById(const std::vector<osi3::MovingObject>& moving_objects,
                                                          OsiId id);

    std::vector<osi3::MovingObject> GetVehicles();

    std::vector<osi3::MovingObject> GetPedestrians();

    const gtgen::core::IHostVehicleInterface& GetHostVehicleInterface() const
    {
        return simulator_->GetHostVehicleInterface();
    }

    const std::vector<osi3::TrafficCommand>& GetTrafficCommands() const { return simulator_->GetTrafficCommands(); }

    void InitialSpin();

    OsiId ObservedEntityStartLaneId() const;

    void CheckAndFailVehicleSpawns();

    void CheckAndFailTrafficVehicleMovement();

    bool CheckTrafficVehicleMoved();

    bool CheckTrafficVehicleIsOnHostVehicleLane();

    bool CheckScenarioCarLaneId(OsiId target_lane_id);

    void WaitForDistance(double expected_offset, std::uint16_t timeout);

    double GetMagnitude(const osi3::Vector3d& v);

    std::uint16_t GetStepSizeMs() const;

    std::chrono::milliseconds GetScenarioDuration() const;

    std::uint8_t expected_total_traffic_vehicles_{0};
    OsiId active_traffic_vehicle_id_{};
    OsiVector traffic_vehicle_start_position_{};
    OsiId previous_scenario_car_lane_id_{};
    OsiId observed_entity_start_lane_id_{};
    const double precision = 1.0e-5;

  private:
    std::unique_ptr<SimulatorImpl> simulator_{};
    std::unique_ptr<TestTrafficUpdate> test_traffic_update_{};

    void SpinOnce();

    bool CheckIfHostVehicleExists();

    void CheckNumberOfTrafficVehicles();

    bool CheckTrafficVehiclePosition(const OsiVector& start_position, const OsiVector& current_position);

    void FindLaneIdOfObservedEntity();

    std::vector<osi3::MovingObject> GetMovingObjectsOfType(osi3::MovingObject_Type type);

    std::uint16_t step_size_ms_{0};
};

}  // namespace gtgen::simulator::test_utils

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_GTGENTESTFIXTURE_GTGENTESTFIXTURE_H
