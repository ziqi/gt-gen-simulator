/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_CREATESIMULATOR_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_CREATESIMULATOR_H

#include "Core/Export/simulation_parameters.h"
#include "Simulator/LibSimCore/simulator_impl.h"

#include <memory>

namespace gtgen::simulator::test_utils
{

inline std::unique_ptr<SimulatorImpl> CreateSimulator(const gtgen::core::SimulationParameters& params)
{
    std::unique_ptr<SimulatorImpl> simulator = std::make_unique<SimulatorImpl>(params);

    return simulator;
}
}  // namespace gtgen::simulator::test_utils

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_TESTUTILS_CREATESIMULATOR_H
