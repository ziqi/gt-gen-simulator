/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_DATA_TESTDATAPATHHANDLER_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_DATA_TESTDATAPATHHANDLER_H

#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/Utils/UserData/data_path_handler.h"

#include <string>

namespace gtgen::simulator::test_data
{

namespace fs = gtgen::core::fs;

inline fs::path GetCustomGtGenDataDirectory(fs::path path = "./Simulator/Tests/Data")
{
    fs::path data_path(std::move(path));
    simulation::user_data::DataPathHandler data_path_handler{};
    data_path_handler.SetGtGenDataDirectory(data_path);
    return data_path_handler.GetGtGenDataDirectory();
}

inline fs::path GetCustomGtGenConfigDirectory()
{
    return GetCustomGtGenDataDirectory() / "UserSettings";
}

}  // namespace gtgen::simulator::test_data

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_TESTS_DATA_TESTDATAPATHHANDLER_H
