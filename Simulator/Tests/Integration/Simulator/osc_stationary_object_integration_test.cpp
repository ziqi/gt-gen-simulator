/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithMapContainingSingleObject_WhenSimulatorSteps_ThenObjectInGroundtruthIsCorrect)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_single_object.xosc"};

    const auto expected_count_of_stationary_object = 1;
    const auto expected_stationary_object_length = 12.10;
    const auto expected_stationary_object_height = 11.00;
    const auto expected_stationary_object_width = 22.40;
    const auto expected_stationary_object_position_x = 30;
    const auto expected_stationary_object_position_y = -6.0;
    const auto expected_stationary_object_position_z = 6.0;
    const auto expected_stationary_object_orientation_roll = 0.0;
    const auto expected_stationary_object_orientation_pitch = 0.0;
    const auto expected_stationary_object_orientation_yaw = 1.57;
    const auto expected_stationary_object_type =
        osi3::StationaryObject::Classification::Type::StationaryObject_Classification_Type_TYPE_BUILDING;
    const auto expected_polygon_size = 4;
    const auto expected_polygon_point_index0_x = 6.05;
    const auto expected_polygon_point_index0_y = 11.20;
    const auto expected_polygon_point_index1_x = -6.05;
    const auto expected_polygon_point_index1_y = 11.20;
    const auto expected_polygon_point_index2_x = -6.05;
    const auto expected_polygon_point_index2_y = -11.20;
    const auto expected_polygon_point_index3_x = 6.05;
    const auto expected_polygon_point_index3_y = -11.20;

    InitSimulator("InternalMovement.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_stationary_object, last_ground_truth.stationary_object_size());

    const auto single_object = last_ground_truth.stationary_object().at(0);

    EXPECT_EQ(single_object.base().dimension().length(), expected_stationary_object_length);
    EXPECT_EQ(single_object.base().dimension().width(), expected_stationary_object_width);
    EXPECT_EQ(single_object.base().dimension().height(), expected_stationary_object_height);

    EXPECT_EQ(single_object.base().position().x(), expected_stationary_object_position_x);
    EXPECT_EQ(single_object.base().position().y(), expected_stationary_object_position_y);
    EXPECT_EQ(single_object.base().position().z(), expected_stationary_object_position_z);

    EXPECT_EQ(single_object.base().orientation().roll(), expected_stationary_object_orientation_roll);
    EXPECT_EQ(single_object.base().orientation().pitch(), expected_stationary_object_orientation_pitch);
    EXPECT_EQ(single_object.base().orientation().yaw(), expected_stationary_object_orientation_yaw);

    EXPECT_EQ(single_object.classification().type(), expected_stationary_object_type);

    EXPECT_EQ(single_object.base().base_polygon_size(), expected_polygon_size);
    const auto base_polygon_groundtruth = single_object.base().base_polygon();
    EXPECT_EQ(base_polygon_groundtruth.at(0).x(), expected_polygon_point_index0_x);
    EXPECT_EQ(base_polygon_groundtruth.at(0).y(), expected_polygon_point_index0_y);
    EXPECT_EQ(base_polygon_groundtruth.at(1).x(), expected_polygon_point_index1_x);
    EXPECT_EQ(base_polygon_groundtruth.at(1).y(), expected_polygon_point_index1_y);
    EXPECT_EQ(base_polygon_groundtruth.at(2).x(), expected_polygon_point_index2_x);
    EXPECT_EQ(base_polygon_groundtruth.at(2).y(), expected_polygon_point_index2_y);
    EXPECT_EQ(base_polygon_groundtruth.at(3).x(), expected_polygon_point_index3_x);
    EXPECT_EQ(base_polygon_groundtruth.at(3).y(), expected_polygon_point_index3_y);
}

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithMapContainingMultipleObjects_WhenSimulatorSteps_ThenGroundtruthContainsAllObjects)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_multiple_objects.xosc"};

    const auto expected_count_of_stationary_objects = 41;

    InitSimulator("IntegrationTestSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_stationary_objects, last_ground_truth.stationary_object_size());
}

}  // namespace gtgen::simulator
