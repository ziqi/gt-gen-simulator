/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using TpmPluginTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

TEST_F(TpmPluginTest, GivenOpenScenarioWithTpmPlugin_WhenSimulatorSteps_ThenGroundTruthSteppedByTpmPlugin)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_tpm_plugin.xosc"};

    // initial position 10m + rear-axis offset 1.4m + 30kmph * 0.1s
    const auto expected_host_vehicle_position_x = 12.2333;

    InitSimulator("TpmPlugIn.ini", osc_file_path, TestHostVehicleMovement::kSimpleStub, 100);
    SpinNTimes(2);  // TPM only steps once
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(1, last_ground_truth.moving_object_size());

    const auto& ego_vehicle = last_ground_truth.moving_object().at(0);
    EXPECT_DOUBLE_EQ(expected_host_vehicle_position_x, ego_vehicle.base().position().x());
}

}  // namespace gtgen::simulator
