/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;
using gtgen::core::test_utils::GetMovingObjectByName;
using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_s;
using units::literals::operator""_ms;
using units::literals::operator""_mps;

class OscSimulatorTest : public test_utils::GtGenTestFixture
{
  protected:
    void Init(const std::string& osc_file_name)
    {
        const fs::path relative_data_path{"./Simulator/Tests/Data"};
        const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
        const fs::path osc_file_path{osc_path / osc_file_name};
        const std::uint16_t step_size_ms = 100;

        InitSimulator("InternalMovement.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, step_size_ms);
    }

    std::optional<mantle_api::Vec3<units::length::meter_t>> GetPedestrianPosition()
    {
        const auto pedestrian = GetMovingObjectByName(GetLastGroundTruth(), "Pedestrian");
        if (pedestrian.has_value())
        {
            return gtgen::core::service::gt_conversion::ToVec3Length(pedestrian->base().position());
        }
        return std::nullopt;
    }
    std::optional<mantle_api::Orientation3<units::angle::radian_t>> GetPedestrianOrientation()
    {
        const auto pedestrian = GetMovingObjectByName(GetLastGroundTruth(), "Pedestrian");
        if (pedestrian.has_value())
        {
            return gtgen::core::service::gt_conversion::ToOrientation3(pedestrian->base().orientation());
        }
        return std::nullopt;
    }

    std::map<units::time::millisecond_t, mantle_api::Vec3<units::length::meter_t>> expected_pedestrian_positions_;
};

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithTrajectoryRouteActionWithTimeReferenceAndLanePosition_WhenInitAndStepSimulatorTillActionIsFinished_ThenPedestrianReachesTheLastTrajectoryPoint)
{

    Init("pedestrian_follow_trajectory_with_time_reference_on_cross_path.xosc");

    expected_pedestrian_positions_.clear();

    expected_pedestrian_positions_.emplace(
        0,
        mantle_api::Vec3<units::length::meter_t>{
            units::length::meter_t(96.2909), units::length::meter_t(61.6505), units::length::meter_t(0.92)});
    expected_pedestrian_positions_.emplace(
        5000,
        mantle_api::Vec3<units::length::meter_t>{
            units::length::meter_t(96.2909), units::length::meter_t(61.6505), units::length::meter_t(0.92)});
    expected_pedestrian_positions_.emplace(
        10000,
        mantle_api::Vec3<units::length::meter_t>{
            units::length::meter_t(97.7528), units::length::meter_t(56.869), units::length::meter_t(0.92)});

    SpinNTimes(1);
    EXPECT_TRIPLE_NEAR(expected_pedestrian_positions_.at(0_ms), GetPedestrianPosition().value(), 0.001);

    RunFor(std::chrono::milliseconds{5000});
    EXPECT_TRIPLE_NEAR(expected_pedestrian_positions_.at(5000_ms), GetPedestrianPosition().value(), 0.001);

    RunFor(std::chrono::milliseconds{5000});
    EXPECT_TRIPLE_NEAR(expected_pedestrian_positions_.at(10000_ms), GetPedestrianPosition().value(), 0.001);
}

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithTrajectoryRouteActionWithTimeReferenceAndLanePosition_WhenInitAndStepSimulatorTillActionFinish_ThenPedestrianIsRemoved)
{

    Init("pedestrian_follow_trajectory_with_time_reference_on_cross_path.xosc");

    expected_pedestrian_positions_.clear();

    expected_pedestrian_positions_.emplace(
        0,
        mantle_api::Vec3<units::length::meter_t>{
            units::length::meter_t(96.2909), units::length::meter_t(61.6505), units::length::meter_t(0.92)});
    SpinNTimes(1);

    EXPECT_TRIPLE_NEAR(expected_pedestrian_positions_.at(0_ms), GetPedestrianPosition().value(), 0.001);

    RunFor(std::chrono::milliseconds{15000});
    EXPECT_TRUE(GetPedestrianPosition().has_value());
}

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithTrajectoryRouteActionWithoutTimeReferenceAndLanePosition_WhenInitAndStepSimulatorTillActionFinish_ThenPedestrianReachesTheLastTrajectoryPoint)
{
    Init("pedestrian_follow_trajectory_without_time_reference_on_crossing_road.xosc");
    expected_pedestrian_positions_.clear();
    const auto pedestrian_velocity_y_ms = 1_mps;
    const auto pedestrian_initial_x = 51.5_m;
    const auto pedestrian_initial_y = -5.0_m;
    const auto pedestrian_initial_z = 0.92_m;
    const auto expected_pedestrian_y_position_at_5_sec =
        static_cast<units::length::meter_t>(pedestrian_initial_y + pedestrian_velocity_y_ms * 5_s);
    const auto expected_pedestrian_y_position_at_10_sec =
        static_cast<units::length::meter_t>(pedestrian_initial_y + pedestrian_velocity_y_ms * 10_s);

    expected_pedestrian_positions_.emplace(
        0, mantle_api::Vec3<units::length::meter_t>{pedestrian_initial_x, pedestrian_initial_y, pedestrian_initial_z});
    expected_pedestrian_positions_.emplace(
        5000,
        mantle_api::Vec3<units::length::meter_t>{
            pedestrian_initial_x, expected_pedestrian_y_position_at_5_sec, pedestrian_initial_z});
    expected_pedestrian_positions_.emplace(
        10000,
        mantle_api::Vec3<units::length::meter_t>{
            pedestrian_initial_x, expected_pedestrian_y_position_at_10_sec, pedestrian_initial_z});
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{3.1408_rad, 0_rad, 0_rad};
    SpinNTimes(1);

    EXPECT_TRIPLE_NEAR(expected_pedestrian_positions_.at(0_ms), GetPedestrianPosition().value(), 0.001);
    EXPECT_TRIPLE(expected_orientation, GetPedestrianOrientation().value());

    RunFor(std::chrono::milliseconds{5000});
    EXPECT_TRIPLE_NEAR(expected_pedestrian_positions_.at(5000_ms), GetPedestrianPosition().value(), 0.001);
    EXPECT_TRIPLE(expected_orientation, GetPedestrianOrientation().value());

    RunFor(std::chrono::milliseconds{5000});
    EXPECT_TRIPLE_NEAR(expected_pedestrian_positions_.at(10000_ms), GetPedestrianPosition().value(), 0.001);
    EXPECT_TRIPLE(expected_orientation, GetPedestrianOrientation().value());
}

}  // namespace gtgen::simulator
