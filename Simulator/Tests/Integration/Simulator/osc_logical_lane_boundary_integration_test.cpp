/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

TEST_F(
    OscSimulatorTest,
    GivenOpenScenarioWithDoubleLinedRoadMarkings_WhenSimulatorSteps_ThenGroundTruthOfLogicalLaneBoundaryIsConvertedCorrectly)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_doublelined_road_markings.xosc"};

    InitSimulator("IntegrationTestSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(last_ground_truth.logical_lane_boundary().size(), 6);
    ASSERT_EQ(last_ground_truth.reference_line_size(), 1);

    const auto expected_reference_line_start_position_x = 0.0;
    const auto expected_reference_line_start_position_y = 0.0;
    const auto expected_reference_line_end_position_x = 100.0;
    const auto expected_reference_line_end_position_y = 0.0;

    const auto expected_lane_minus_1_left_boundary_start_position_x = 0.0;
    const auto expected_lane_minus_1_left_boundary_start_position_y = 0.0;
    const auto expected_lane_minus_1_left_boundary_end_position_x = 100.0;
    const auto expected_lane_minus_1_left_boundary_end_position_y = 0.0;
    const auto expected_lane_minus_1_left_boundary_passing_rule =
        osi3::LogicalLaneBoundary::PassingRule::LogicalLaneBoundary_PassingRule_PASSING_RULE_DECREASING_T;

    const auto expected_lane_mimus_1_right_boundary_start_position_x = 0.0;
    const auto expected_lane_mimus_1_right_boundary_start_position_y = -3.0;
    const auto expected_lane_mimus_1_right_boundary_end_position_x = 100.0;
    const auto expected_lane_mimus_1_right_boundary_end_position_y = -3.0;
    const auto expected_lane_mimus_1_right_boundary_passing_rule =
        osi3::LogicalLaneBoundary::PassingRule::LogicalLaneBoundary_PassingRule_PASSING_RULE_DECREASING_T;

    const auto expected_lane_mimus_2_right_boundary_start_position_x = 0.0;
    const auto expected_lane_mimus_2_right_boundary_start_position_y = -6.0;
    const auto expected_lane_mimus_2_right_boundary_end_position_x = 100.0;
    const auto expected_lane_mimus_2_right_boundary_end_position_y = -6.0;
    const auto expected_lane_mimus_2_right_boundary_passing_rule =
        osi3::LogicalLaneBoundary::PassingRule::LogicalLaneBoundary_PassingRule_PASSING_RULE_NONE_ALLOWED;

    const auto expected_lane_1_left_boundary_start_position_x = 0.0;
    const auto expected_lane_1_left_boundary_start_position_y = 0.0;
    const auto expected_lane_1_left_boundary_end_position_x = 100.0;
    const auto expected_lane_1_left_boundary_end_position_y = 0.0;
    const auto expected_lane_1_left_boundary_passing_rule =
        osi3::LogicalLaneBoundary::PassingRule::LogicalLaneBoundary_PassingRule_PASSING_RULE_DECREASING_T;

    const auto expected_lane_1_right_boundary_start_position_x = 100.0;
    const auto expected_lane_1_right_boundary_start_position_y = 3.0;
    const auto expected_lane_1_right_boundary_end_position_x = 0.0;
    const auto expected_lane_1_right_boundary_end_position_y = 3.0;
    const auto expected_lane_1_right_boundary_passing_rule =
        osi3::LogicalLaneBoundary::PassingRule::LogicalLaneBoundary_PassingRule_PASSING_RULE_DECREASING_T;

    const auto expected_lane_2_right_boundary_start_position_x = 100.0;
    const auto expected_lane_2_right_boundary_start_position_y = 6.0;
    const auto expected_lane_2_right_boundary_end_position_x = 0.0;
    const auto expected_lane_2_right_boundary_end_position_y = 6.0;
    const auto expected_lane_2_right_boundary_passing_rule =
        osi3::LogicalLaneBoundary::PassingRule::LogicalLaneBoundary_PassingRule_PASSING_RULE_NONE_ALLOWED;

    EXPECT_EQ(last_ground_truth.reference_line().begin()->poly_line().begin()->world_position().x(),
              expected_reference_line_start_position_x);
    EXPECT_EQ(last_ground_truth.reference_line().begin()->poly_line().begin()->world_position().y(),
              expected_reference_line_start_position_y);
    EXPECT_EQ(last_ground_truth.reference_line().begin()->poly_line().rbegin()->world_position().x(),
              expected_reference_line_end_position_x);
    EXPECT_EQ(last_ground_truth.reference_line().begin()->poly_line().rbegin()->world_position().y(),
              expected_reference_line_end_position_y);

    const auto lane_minus_1_left_logic_boundary = last_ground_truth.logical_lane_boundary().at(2);
    const auto lane_mimus_1_right_logic_boundary = last_ground_truth.logical_lane_boundary().at(0);
    const auto lane_mimus_2_right_logic_boundary = last_ground_truth.logical_lane_boundary().at(1);
    const auto lane_1_left_logic_boundary = last_ground_truth.logical_lane_boundary().at(5);
    const auto lane_1_right_logic_boundary = last_ground_truth.logical_lane_boundary().at(3);
    const auto lane_2_right_logic_boundary = last_ground_truth.logical_lane_boundary().at(4);

    EXPECT_EQ(lane_minus_1_left_logic_boundary.boundary_line().begin()->position().x(),
              expected_lane_minus_1_left_boundary_start_position_x);
    EXPECT_EQ(lane_minus_1_left_logic_boundary.boundary_line().begin()->position().y(),
              expected_lane_minus_1_left_boundary_start_position_y);
    EXPECT_EQ(lane_minus_1_left_logic_boundary.boundary_line().rbegin()->position().x(),
              expected_lane_minus_1_left_boundary_end_position_x);
    EXPECT_EQ(lane_minus_1_left_logic_boundary.boundary_line().rbegin()->position().y(),
              expected_lane_minus_1_left_boundary_end_position_y);
    EXPECT_EQ(lane_minus_1_left_logic_boundary.reference_line_id(), last_ground_truth.reference_line().at(0).id());
    EXPECT_EQ(lane_minus_1_left_logic_boundary.passing_rule(), expected_lane_minus_1_left_boundary_passing_rule);
    ASSERT_EQ(lane_minus_1_left_logic_boundary.physical_boundary_id_size(), 2);

    EXPECT_EQ(lane_mimus_1_right_logic_boundary.boundary_line().begin()->position().x(),
              expected_lane_mimus_1_right_boundary_start_position_x);
    EXPECT_EQ(lane_mimus_1_right_logic_boundary.boundary_line().begin()->position().y(),
              expected_lane_mimus_1_right_boundary_start_position_y);
    EXPECT_EQ(lane_mimus_1_right_logic_boundary.boundary_line().rbegin()->position().x(),
              expected_lane_mimus_1_right_boundary_end_position_x);
    EXPECT_EQ(lane_mimus_1_right_logic_boundary.boundary_line().rbegin()->position().y(),
              expected_lane_mimus_1_right_boundary_end_position_y);
    EXPECT_EQ(lane_mimus_1_right_logic_boundary.reference_line_id(), last_ground_truth.reference_line().at(0).id());
    EXPECT_EQ(lane_mimus_1_right_logic_boundary.passing_rule(), expected_lane_mimus_1_right_boundary_passing_rule);
    ASSERT_EQ(lane_mimus_1_right_logic_boundary.physical_boundary_id_size(), 2);

    EXPECT_EQ(lane_mimus_2_right_logic_boundary.boundary_line().begin()->position().x(),
              expected_lane_mimus_2_right_boundary_start_position_x);
    EXPECT_EQ(lane_mimus_2_right_logic_boundary.boundary_line().begin()->position().y(),
              expected_lane_mimus_2_right_boundary_start_position_y);
    EXPECT_EQ(lane_mimus_2_right_logic_boundary.boundary_line().rbegin()->position().x(),
              expected_lane_mimus_2_right_boundary_end_position_x);
    EXPECT_EQ(lane_mimus_2_right_logic_boundary.boundary_line().rbegin()->position().y(),
              expected_lane_mimus_2_right_boundary_end_position_y);
    EXPECT_EQ(lane_mimus_2_right_logic_boundary.reference_line_id(), last_ground_truth.reference_line().at(0).id());
    EXPECT_EQ(lane_mimus_2_right_logic_boundary.passing_rule(), expected_lane_mimus_2_right_boundary_passing_rule);
    ASSERT_EQ(lane_mimus_2_right_logic_boundary.physical_boundary_id_size(), 1);

    EXPECT_EQ(lane_1_left_logic_boundary.boundary_line().begin()->position().x(),
              expected_lane_1_left_boundary_start_position_x);
    EXPECT_EQ(lane_1_left_logic_boundary.boundary_line().begin()->position().y(),
              expected_lane_1_left_boundary_start_position_y);
    EXPECT_EQ(lane_1_left_logic_boundary.boundary_line().rbegin()->position().x(),
              expected_lane_1_left_boundary_end_position_x);
    EXPECT_EQ(lane_1_left_logic_boundary.boundary_line().rbegin()->position().y(),
              expected_lane_1_left_boundary_end_position_y);
    EXPECT_EQ(lane_1_left_logic_boundary.reference_line_id(), last_ground_truth.reference_line().at(0).id());
    EXPECT_EQ(lane_1_left_logic_boundary.passing_rule(), expected_lane_1_left_boundary_passing_rule);
    ASSERT_EQ(lane_1_left_logic_boundary.physical_boundary_id_size(), 2);

    EXPECT_EQ(lane_1_right_logic_boundary.boundary_line().begin()->position().x(),
              expected_lane_1_right_boundary_start_position_x);
    EXPECT_EQ(lane_1_right_logic_boundary.boundary_line().begin()->position().y(),
              expected_lane_1_right_boundary_start_position_y);
    EXPECT_EQ(lane_1_right_logic_boundary.boundary_line().rbegin()->position().x(),
              expected_lane_1_right_boundary_end_position_x);
    EXPECT_EQ(lane_1_right_logic_boundary.boundary_line().rbegin()->position().y(),
              expected_lane_1_right_boundary_end_position_y);
    EXPECT_EQ(lane_1_right_logic_boundary.reference_line_id(), last_ground_truth.reference_line().at(0).id());
    EXPECT_EQ(lane_1_right_logic_boundary.passing_rule(), expected_lane_1_right_boundary_passing_rule);
    ASSERT_EQ(lane_1_right_logic_boundary.physical_boundary_id_size(), 2);

    EXPECT_EQ(lane_2_right_logic_boundary.boundary_line().begin()->position().x(),
              expected_lane_2_right_boundary_start_position_x);
    EXPECT_EQ(lane_2_right_logic_boundary.boundary_line().begin()->position().y(),
              expected_lane_2_right_boundary_start_position_y);
    EXPECT_EQ(lane_2_right_logic_boundary.boundary_line().rbegin()->position().x(),
              expected_lane_2_right_boundary_end_position_x);
    EXPECT_EQ(lane_2_right_logic_boundary.boundary_line().rbegin()->position().y(),
              expected_lane_2_right_boundary_end_position_y);
    EXPECT_EQ(lane_2_right_logic_boundary.reference_line_id(), last_ground_truth.reference_line().at(0).id());
    EXPECT_EQ(lane_2_right_logic_boundary.passing_rule(), expected_lane_2_right_boundary_passing_rule);
    ASSERT_EQ(lane_2_right_logic_boundary.physical_boundary_id_size(), 1);
    EXPECT_EQ(lane_2_right_logic_boundary.physical_boundary_id().at(0).value(), 24);
}

}  // namespace gtgen::simulator
