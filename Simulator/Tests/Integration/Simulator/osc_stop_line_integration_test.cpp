/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

TEST_F(OscSimulatorTest, GivenOpenScenarioWithStopLine_WhenSimulatorSteps_ThenGroundTruthContainsStopLineAsRoadMarking)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_stop_line.xosc"};

    const auto expected_count_of_road_markings = 1;

    // TrafficSign_MainSign_Classification_Type_TYPE_STOP = 17
    const auto expected_road_marking_sign_type = 17;
    // RoadMarking_Classification_Type_TYPE_SYMBOLIC_TRAFFIC_SIGN = 3
    const auto expected_road_marking_type = 3;
    const auto expected_road_marking_sign_value = -1;

    InitSimulator("InternalMovement.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_road_markings, last_ground_truth.road_marking_size());

    EXPECT_EQ(expected_road_marking_sign_type,
              last_ground_truth.road_marking(0).classification().traffic_main_sign_type());
    EXPECT_EQ(expected_road_marking_type, last_ground_truth.road_marking(0).classification().type());
    EXPECT_EQ(expected_road_marking_sign_value, last_ground_truth.road_marking(0).classification().value().value());

    EXPECT_EQ(last_ground_truth.road_marking(0).classification().assigned_lane_id().size(), 1);
    EXPECT_EQ(last_ground_truth.road_marking(0).classification().assigned_lane_id().Get(0).value(),
              last_ground_truth.lane(0).id().value());
}

}  // namespace gtgen::simulator
