/*******************************************************************************
 * Copyright (c) 2024-2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithMountedSign_WhenSimulatorSteps_ThenGroundTruthContainsMountedSignCorrectly)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_supplementary_sign_entity.xosc"};

    // mounted sign is the combination of a main sign and supplementary signs.
    const auto expected_count_of_mounted_signs = 1;
    const auto expected_count_of_supplementary_signs = 1;

    const auto expected_supplementary_sign_length = 0.1;
    const auto expected_supplementary_sign_height = 0.2;
    const auto expected_supplementary_sign_width = 0.2;
    const auto expected_supplementary_sign_position_z = 1.6;
    const auto expected_supplementary_sign_orientation_roll = 0.0;
    const auto expected_supplementary_sign_orientation_pitch = 0.0;
    const auto expected_supplementary_sign_orientation_yaw = 3.1415;
    const auto expected_supplementary_sign_type = osi3::TrafficSign::SupplementarySign::Classification::Type::
        TrafficSign_SupplementarySign_Classification_Type_TYPE_CONSTRAINED_TO;
    const auto expected_supplementary_sign_actor = osi3::TrafficSign::SupplementarySign::Classification::Actor::
        TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS;

    InitSimulator("InternalMovement.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_mounted_signs, last_ground_truth.traffic_sign_size());
    ASSERT_TRUE(last_ground_truth.traffic_sign(0).has_main_sign());
    auto actual_count_of_supplementary_signs = 0;

    for (const auto& traffic_sign : last_ground_truth.traffic_sign())
    {
        actual_count_of_supplementary_signs += traffic_sign.supplementary_sign_size();
    }

    ASSERT_EQ(expected_count_of_supplementary_signs, actual_count_of_supplementary_signs);

    const auto supplementary_sign = last_ground_truth.traffic_sign(0).supplementary_sign(0);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().length(), expected_supplementary_sign_length);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().width(), expected_supplementary_sign_width);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().height(), expected_supplementary_sign_height);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().position().z(), expected_supplementary_sign_position_z);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().roll(), expected_supplementary_sign_orientation_roll);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().pitch(), expected_supplementary_sign_orientation_pitch);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().yaw(), expected_supplementary_sign_orientation_yaw);

    EXPECT_EQ(supplementary_sign.classification().type(), expected_supplementary_sign_type);
    ASSERT_EQ(supplementary_sign.classification().actor_size(), 1);
    EXPECT_EQ(supplementary_sign.classification().actor().at(0), expected_supplementary_sign_actor);
}

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithVariantMountedSign_WhenSimulatorSteps_ThenGroundTruthContainsMountedSignCorrectly)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_supplementary_sign_entity_variation.xosc"};

    // mounted sign is the combination of a main sign and supplementary signs.
    const auto expected_count_of_mounted_signs = 1;
    const auto expected_count_of_supplementary_signs = 3;

    const auto expected_supplementary_sign_length = 0.1;
    const auto expected_supplementary_sign_height = 0.2;
    const auto expected_supplementary_sign_width = 0.2;
    const auto expected_supplementary_sign_position_z = 1.6;
    const auto expected_supplementary_sign_orientation_roll = 0.0;
    const auto expected_supplementary_sign_orientation_pitch = 0.0;
    const auto expected_supplementary_sign_orientation_yaw = 3.1415;
    const auto expected_supplementary_sign_type = osi3::TrafficSign::SupplementarySign::Classification::Type::
        TrafficSign_SupplementarySign_Classification_Type_TYPE_TEXT;
    const auto expected_supplementary_sign_actor = osi3::TrafficSign::SupplementarySign::Classification::Actor::
        TrafficSign_SupplementarySign_Classification_Actor_ACTOR_UNKNOWN;

    InitSimulator("InternalMovement.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_mounted_signs, last_ground_truth.traffic_sign_size());
    ASSERT_TRUE(last_ground_truth.traffic_sign(0).has_main_sign());
    auto actual_count_of_supplementary_signs = 0;

    for (const auto& traffic_sign : last_ground_truth.traffic_sign())
    {
        actual_count_of_supplementary_signs += traffic_sign.supplementary_sign_size();
    }

    ASSERT_EQ(expected_count_of_supplementary_signs, actual_count_of_supplementary_signs);

    const auto supplementary_sign = last_ground_truth.traffic_sign(0).supplementary_sign(0);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().length(), expected_supplementary_sign_length);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().width(), expected_supplementary_sign_width);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().height(), expected_supplementary_sign_height);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().position().z(), expected_supplementary_sign_position_z);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().roll(), expected_supplementary_sign_orientation_roll);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().pitch(), expected_supplementary_sign_orientation_pitch);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().yaw(), expected_supplementary_sign_orientation_yaw);

    EXPECT_EQ(supplementary_sign.classification().type(), expected_supplementary_sign_type);
    ASSERT_EQ(supplementary_sign.classification().actor_size(), 1);
    EXPECT_EQ(supplementary_sign.classification().actor().at(0), expected_supplementary_sign_actor);
}

TEST_F(
    OscSimulatorTest,
    GivenScenarioWithSupplementarySignWithoutTeleportAction_WhenSimulatorSteps_ThenSupplementarySignContainsExpectedPose)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_supplementary_sign_entity_without_teleport_action.xosc"};

    // mounted sign is the combination of a main sign and supplementary signs.
    const auto expected_count_of_mounted_signs = 1;
    const auto expected_count_of_supplementary_signs = 1;

    const auto expected_supplementary_sign_length = 0.1;
    const auto expected_supplementary_sign_height = 0.2;
    const auto expected_supplementary_sign_width = 0.2;
    const auto expected_supplementary_sign_position_x = 48.0;
    const auto expected_supplementary_sign_position_y = -1.5;
    const auto expected_supplementary_sign_position_z = 1.6;
    const auto expected_supplementary_sign_orientation_roll = 0.0;
    const auto expected_supplementary_sign_orientation_pitch = 0.0;
    const auto expected_supplementary_sign_orientation_yaw = 3.1415;
    const auto expected_supplementary_sign_type = osi3::TrafficSign::SupplementarySign::Classification::Type::
        TrafficSign_SupplementarySign_Classification_Type_TYPE_CONSTRAINED_TO;
    const auto expected_supplementary_sign_actor = osi3::TrafficSign::SupplementarySign::Classification::Actor::
        TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS;

    InitSimulator("InternalMovement.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_mounted_signs, last_ground_truth.traffic_sign_size());
    ASSERT_TRUE(last_ground_truth.traffic_sign(0).has_main_sign());
    auto actual_count_of_supplementary_signs = 0;

    for (const auto& traffic_sign : last_ground_truth.traffic_sign())
    {
        actual_count_of_supplementary_signs += traffic_sign.supplementary_sign_size();
    }

    ASSERT_EQ(expected_count_of_supplementary_signs, actual_count_of_supplementary_signs);

    const auto supplementary_sign = last_ground_truth.traffic_sign(0).supplementary_sign(0);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().length(), expected_supplementary_sign_length);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().width(), expected_supplementary_sign_width);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().height(), expected_supplementary_sign_height);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().position().x(), expected_supplementary_sign_position_x);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().position().y(), expected_supplementary_sign_position_y);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().position().z(), expected_supplementary_sign_position_z);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().roll(), expected_supplementary_sign_orientation_roll);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().pitch(), expected_supplementary_sign_orientation_pitch);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().yaw(), expected_supplementary_sign_orientation_yaw);

    EXPECT_EQ(supplementary_sign.classification().type(), expected_supplementary_sign_type);
    ASSERT_EQ(supplementary_sign.classification().actor_size(), 1);
    EXPECT_EQ(supplementary_sign.classification().actor().at(0), expected_supplementary_sign_actor);
}

// The test has been temporarily disabled due to a missing feature following a recent fix in GTGen Core.
// Refer to https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/issues/9.
TEST_F(
    OscSimulatorTest,
    DISABLED_GivenScenarioWithSupplementarySignWithoutTeleportActionInElevatedRoad_WhenSimulatorSteps_ThenSupplementarySignContainsExpectedPose)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path /
                                 "scenario_with_supplementary_sign_entity_without_teleport_action_variation.xosc"};

    // mounted sign is the combination of a main sign and supplementary signs.
    const auto expected_count_of_mounted_signs = 1;
    const auto expected_count_of_supplementary_signs = 1;

    const auto expected_supplementary_sign_length = 0.1;
    const auto expected_supplementary_sign_height = 0.2;
    const auto expected_supplementary_sign_width = 0.2;
    const auto expected_supplementary_sign_position_x = 29.88;
    const auto expected_supplementary_sign_position_y = -6.0;
    const auto expected_supplementary_sign_position_z = 7.61;
    const auto expected_supplementary_sign_type = osi3::TrafficSign::SupplementarySign::Classification::Type::
        TrafficSign_SupplementarySign_Classification_Type_TYPE_CONSTRAINED_TO;
    const auto expected_supplementary_sign_actor = osi3::TrafficSign::SupplementarySign::Classification::Actor::
        TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS;

    InitSimulator("UserSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_mounted_signs, last_ground_truth.traffic_sign_size());
    ASSERT_TRUE(last_ground_truth.traffic_sign(0).has_main_sign());
    auto actual_count_of_supplementary_signs = 0;

    for (const auto& traffic_sign : last_ground_truth.traffic_sign())
    {
        actual_count_of_supplementary_signs += traffic_sign.supplementary_sign_size();
    }

    ASSERT_EQ(expected_count_of_supplementary_signs, actual_count_of_supplementary_signs);

    const auto main_sign = last_ground_truth.traffic_sign(0).main_sign();
    const auto supplementary_sign = last_ground_truth.traffic_sign(0).supplementary_sign(0);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().length(), expected_supplementary_sign_length);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().width(), expected_supplementary_sign_width);
    EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().height(), expected_supplementary_sign_height);

    EXPECT_NEAR(supplementary_sign.base().position().x(), expected_supplementary_sign_position_x, 1e-2);
    EXPECT_NEAR(supplementary_sign.base().position().y(), expected_supplementary_sign_position_y, 1e-2);
    EXPECT_NEAR(supplementary_sign.base().position().z(), expected_supplementary_sign_position_z, 1e-2);

    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().roll(), main_sign.base().orientation().roll());
    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().pitch(), main_sign.base().orientation().pitch());
    EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().yaw(), main_sign.base().orientation().yaw());

    EXPECT_EQ(supplementary_sign.classification().type(), expected_supplementary_sign_type);
    ASSERT_EQ(supplementary_sign.classification().actor_size(), 1);
    EXPECT_EQ(supplementary_sign.classification().actor().at(0), expected_supplementary_sign_actor);
}

TEST_F(
    OscSimulatorTest,
    GivenScenarioWithSupplementarySignsWithoutTeleportAction_WhenSimulatorSteps_ThenSupplementarySignsContainExpectedPose)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_supplementary_sign_entities_without_teleport_action.xosc"};

    // mounted sign is the combination of a main sign and supplementary signs.
    const auto expected_count_of_mounted_signs = 1;
    const auto expected_count_of_supplementary_signs = 3;

    const auto expected_supplementary_sign_length = 0.1;
    const auto expected_supplementary_sign_height = 0.2;
    const auto expected_supplementary_sign_width = 0.2;
    const auto expected_supplementary_sign_position_x = 48.0;
    const auto expected_supplementary_sign_position_y = -1.5;
    const auto expected_supplementary_sign_position_z = 1.6;
    const auto expected_supplementary_sign_orientation_roll = 0.0;
    const auto expected_supplementary_sign_orientation_pitch = 0.0;
    const auto expected_supplementary_sign_orientation_yaw = 3.1415;

    InitSimulator("InternalMovement.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(expected_count_of_mounted_signs, last_ground_truth.traffic_sign_size());
    ASSERT_TRUE(last_ground_truth.traffic_sign(0).has_main_sign());
    auto actual_count_of_supplementary_signs = 0;

    for (const auto& traffic_sign : last_ground_truth.traffic_sign())
    {
        actual_count_of_supplementary_signs += traffic_sign.supplementary_sign_size();
    }

    ASSERT_EQ(expected_count_of_supplementary_signs, actual_count_of_supplementary_signs);

    for (const auto& supplementary_sign : last_ground_truth.traffic_sign(0).supplementary_sign())
    {
        EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().length(), expected_supplementary_sign_length);
        EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().width(), expected_supplementary_sign_width);
        EXPECT_DOUBLE_EQ(supplementary_sign.base().dimension().height(), expected_supplementary_sign_height);

        EXPECT_DOUBLE_EQ(supplementary_sign.base().position().x(), expected_supplementary_sign_position_x);
        EXPECT_DOUBLE_EQ(supplementary_sign.base().position().y(), expected_supplementary_sign_position_y);
        EXPECT_DOUBLE_EQ(supplementary_sign.base().position().z(), expected_supplementary_sign_position_z);

        EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().roll(), expected_supplementary_sign_orientation_roll);
        EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().pitch(),
                         expected_supplementary_sign_orientation_pitch);
        EXPECT_DOUBLE_EQ(supplementary_sign.base().orientation().yaw(), expected_supplementary_sign_orientation_yaw);
    }
}

}  // namespace gtgen::simulator
