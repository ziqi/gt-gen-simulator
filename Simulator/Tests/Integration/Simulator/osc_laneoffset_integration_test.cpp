/*******************************************************************************
 * Copyright (c) 2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/Tests/TestUtils/GtGenTestFixture/gtgen_test_fixture.h"

namespace gtgen::simulator
{
namespace fs = gtgen::core::fs;
using OscSimulatorTest = test_utils::GtGenTestFixture;
using TestHostVehicleMovement = test_utils::TestTrafficUpdate::TestHostVehicleMovement;
static const double kAbsErrorMax = 1e-6;

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithLaneOffset_WhenSimulatorSteps_ThenGroundTruthOfLogicalLaneBoundaryIsConvertedCorrectly)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_laneoffset.xosc"};

    InitSimulator("IntegrationTestSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(last_ground_truth.logical_lane_boundary().size(), 4);
    const auto& logical_lane_boundaries = last_ground_truth.logical_lane_boundary();

    const auto& lane_s0_right_logical_boundary = logical_lane_boundaries[0];
    const auto& lane_s50_right_logical_boundary = logical_lane_boundaries[1];
    const auto& lane_s0_left_logical_boundary = logical_lane_boundaries[2];
    const auto& lane_s50_left_logical_boundary = logical_lane_boundaries[3];

    EXPECT_NEAR(lane_s0_right_logical_boundary.boundary_line().begin()->position().x(), 100.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_right_logical_boundary.boundary_line().begin()->position().y(), -3.2, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_right_logical_boundary.boundary_line().rbegin()->position().x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_right_logical_boundary.boundary_line().rbegin()->position().y(), -3.1, kAbsErrorMax);

    EXPECT_NEAR(lane_s0_left_logical_boundary.boundary_line().begin()->position().x(), 100.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_left_logical_boundary.boundary_line().begin()->position().y(), 0.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_left_logical_boundary.boundary_line().rbegin()->position().x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_left_logical_boundary.boundary_line().rbegin()->position().y(), 0.1, kAbsErrorMax);

    EXPECT_NEAR(lane_s50_right_logical_boundary.boundary_line().begin()->position().x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_right_logical_boundary.boundary_line().begin()->position().y(), -3.1, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_right_logical_boundary.boundary_line().rbegin()->position().x(), 200.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_right_logical_boundary.boundary_line().rbegin()->position().y(), 0.15, kAbsErrorMax);

    EXPECT_NEAR(lane_s50_left_logical_boundary.boundary_line().begin()->position().x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_left_logical_boundary.boundary_line().begin()->position().y(), 0.1, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_left_logical_boundary.boundary_line().rbegin()->position().x(), 200.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_left_logical_boundary.boundary_line().rbegin()->position().y(), 3.35, kAbsErrorMax);
}

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithLaneOffset_WhenSimulatorSteps_ThenGroundTruthOfLaneBoundariesIsConvertedCorrectly)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_laneoffset.xosc"};

    InitSimulator("IntegrationTestSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(last_ground_truth.lane_boundary().size(), 4);
    const auto& lane_boundaries = last_ground_truth.lane_boundary();

    const auto& lane_s0_right_boundary = lane_boundaries[0];
    const auto& lane_s50_right_boundary = lane_boundaries[2];
    const auto& lane_s0_left_boundary = lane_boundaries[1];
    const auto& lane_s50_left_boundary = lane_boundaries[3];

    EXPECT_NEAR(lane_s0_right_boundary.boundary_line().begin()->position().x(), 100.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_right_boundary.boundary_line().begin()->position().y(), -3.2, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_right_boundary.boundary_line().rbegin()->position().x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_right_boundary.boundary_line().rbegin()->position().y(), -3.1, kAbsErrorMax);

    EXPECT_NEAR(lane_s0_left_boundary.boundary_line().begin()->position().x(), 100.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_left_boundary.boundary_line().begin()->position().y(), 0.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_left_boundary.boundary_line().rbegin()->position().x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_left_boundary.boundary_line().rbegin()->position().y(), 0.1, kAbsErrorMax);

    EXPECT_NEAR(lane_s50_right_boundary.boundary_line().begin()->position().x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_right_boundary.boundary_line().begin()->position().y(), -3.1, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_right_boundary.boundary_line().rbegin()->position().x(), 200.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_right_boundary.boundary_line().rbegin()->position().y(), 0.15, kAbsErrorMax);

    EXPECT_NEAR(lane_s50_left_boundary.boundary_line().begin()->position().x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_left_boundary.boundary_line().begin()->position().y(), 0.1, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_left_boundary.boundary_line().rbegin()->position().x(), 200.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_left_boundary.boundary_line().rbegin()->position().y(), 3.35, kAbsErrorMax);
}

TEST_F(OscSimulatorTest,
       GivenOpenScenarioWithLaneOffset_WhenSimulatorSteps_ThenGroundTruthOfCenterlinesIsConvertedCorrectly)
{
    const fs::path relative_data_path{"./Simulator/Tests/Data"};
    const fs::path osc_path{relative_data_path / "Scenarios/XOSC/"};
    const fs::path osc_file_path{osc_path / "scenario_with_laneoffset.xosc"};

    InitSimulator("IntegrationTestSettings.ini", osc_file_path, TestHostVehicleMovement::kInternalMovement, 100);
    SpinNTimes(1);
    const auto last_ground_truth = GetLastGroundTruth();

    ASSERT_EQ(last_ground_truth.lane().size(), 2);

    const auto& lane_s0_centerline = last_ground_truth.lane().begin()->classification().centerline();
    const auto& lane_s50_centerline = last_ground_truth.lane().rbegin()->classification().centerline();

    EXPECT_NEAR(lane_s0_centerline.begin()->x(), 100.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_centerline.begin()->y(), -1.6, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_centerline.rbegin()->x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s0_centerline.rbegin()->y(), -1.5, kAbsErrorMax);

    EXPECT_NEAR(lane_s50_centerline.begin()->x(), 150.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_centerline.begin()->y(), -1.5, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_centerline.rbegin()->x(), 200.0, kAbsErrorMax);
    EXPECT_NEAR(lane_s50_centerline.rbegin()->y(), 1.75, kAbsErrorMax);
}

}  // namespace gtgen::simulator
