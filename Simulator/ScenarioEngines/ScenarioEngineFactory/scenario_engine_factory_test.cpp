/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/ScenarioEngines/ScenarioEngineFactory/scenario_engine_factory.h"

#include "Core/Simulation/Simulator/exception.h"
#include "Simulator/ScenarioEngines/OscScenarioEngine/Engine/osc_scenario_engine.h"
#include "Simulator/Utils/UserData/user_data_manager.h"

#include <MantleAPI/Execution/i_scenario_engine.h>
#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <units.h>

namespace gtgen::simulator
{

using gtgen::core::simulation::simulator::SimulatorException;

TEST(ScenarioFactoryTest, GivenOSCScenario_WhenCreatingEngine_ThenOSCScenarioEngineCreated)
{
    simulation::user_data::UserDataManager user_data_manager{"", ""};
    auto concrete_engine = ScenarioEngineFactory::Create(
        "my/fantasy/path/scenario.xosc", std::make_shared<mantle_api::MockEnvironment>(), &user_data_manager);
    osc_scenario_engine::OSCScenarioEngine* osc_engine =
        dynamic_cast<osc_scenario_engine::OSCScenarioEngine*>(concrete_engine.get());
    EXPECT_TRUE(osc_engine != nullptr);
}

TEST(ScenarioFactoryTest, GivenUnkownScenario_WhenCreatingEngine_ThenExceptionIsThrown)
{
    simulation::user_data::UserDataManager user_data_manager{"", ""};
    EXPECT_THROW(ScenarioEngineFactory::Create("my/fantasy/path/scenario.fantasyextension",
                                               std::make_shared<mantle_api::MockEnvironment>(),
                                               &user_data_manager),
                 SimulatorException);
}

}  // namespace gtgen::simulator
