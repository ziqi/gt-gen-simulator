/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Simulator/ScenarioEngines/OscScenarioEngine/Service/Logging/osc_engine_logger.h"

#include "Simulator/Utils/Logging/logging.h"

namespace gtgen::simulator::osc_scenario_engine
{

mantle_api::LogLevel OscEngineLogger::GetCurrentLogLevel() const noexcept
{
    return mantle_api::LogLevel::kTrace;
}

void OscEngineLogger::Log(mantle_api::LogLevel level, std::string_view message) noexcept
{
    ASSERT(level <= mantle_api::LogLevel::kCritical);

    switch (level)
    {
        case mantle_api::LogLevel::kTrace:
            TRACE(message);
            break;
        case mantle_api::LogLevel::kDebug:
            osc_scenario_engine::Debug(message);
            break;
        case mantle_api::LogLevel::kInfo:
            osc_scenario_engine::Info(message);
            break;
        case mantle_api::LogLevel::kWarning:
            osc_scenario_engine::Warn(message);
            break;
        case mantle_api::LogLevel::kError:
            [[fallthrough]];
        case mantle_api::LogLevel::kCritical:
            osc_scenario_engine::Error(message);
            break;
        default:
            break;
    }
}

}  // namespace gtgen::simulator::osc_scenario_engine
