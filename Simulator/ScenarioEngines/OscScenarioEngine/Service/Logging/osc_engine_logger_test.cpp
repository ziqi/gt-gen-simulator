/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/ScenarioEngines/OscScenarioEngine/Service/Logging/osc_engine_logger.h"

#include "Core/Service/Logging/log_setup.h"

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

namespace gtgen::simulator::osc_scenario_engine
{

TEST(MantleLoggerTest, GivenLogger_WhenGetCurrentLogLevel_ThenReturnTrace)
{
    OscEngineLogger logger;
    EXPECT_EQ(logger.GetCurrentLogLevel(), mantle_api::LogLevel::kTrace);
}

class MantleLoggerTestFixture : public ::testing::TestWithParam<mantle_api::LogLevel>
{
  protected:
    OscEngineLogger logger_;
    const std::string_view message{"Message"};
};

#ifndef NDEBUG  // no trace output on release builds
INSTANTIATE_TEST_SUITE_P(LogLevels,
                         MantleLoggerTestFixture,
                         ::testing::ValuesIn(std::vector<mantle_api::LogLevel>{mantle_api::LogLevel::kTrace,
                                                                               mantle_api::LogLevel::kDebug,
                                                                               mantle_api::LogLevel::kInfo,
                                                                               mantle_api::LogLevel::kWarning,
                                                                               mantle_api::LogLevel::kError,
                                                                               mantle_api::LogLevel::kCritical}));
#else
INSTANTIATE_TEST_SUITE_P(LogLevels,
                         MantleLoggerTestFixture,
                         ::testing::ValuesIn(std::vector<mantle_api::LogLevel>{mantle_api::LogLevel::kDebug,
                                                                               mantle_api::LogLevel::kInfo,
                                                                               mantle_api::LogLevel::kWarning,
                                                                               mantle_api::LogLevel::kError,
                                                                               mantle_api::LogLevel::kCritical}));
#endif

TEST_P(MantleLoggerTestFixture, GivenLogger_WhenCallLog_ThenMessageLogged)
{
    gtgen::core::service::logging::LogSetup::Instance().AddConsoleLogger("trace");

    testing::internal::CaptureStdout();

    auto log_level = GetParam();
    EXPECT_NO_THROW(logger_.Log(log_level, message));

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr(message));
}

}  // namespace gtgen::simulator::osc_scenario_engine
