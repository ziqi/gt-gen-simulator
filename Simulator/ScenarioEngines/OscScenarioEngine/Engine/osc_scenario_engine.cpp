/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Simulator/ScenarioEngines/OscScenarioEngine/Engine/osc_scenario_engine.h"

#include "Simulator/ScenarioEngines/OscScenarioEngine/Service/Logging/osc_engine_logger.h"
#include "Simulator/Utils/Logging/logging.h"

namespace gtgen::simulator::osc_scenario_engine
{

OSCScenarioEngine::OSCScenarioEngine(const std::string& scenario_file_path,
                                     const simulation::user_data::DataPathHandler& data_path_handler,
                                     std::shared_ptr<mantle_api::IEnvironment> environment)
    : OpenScenarioEngine(scenario_file_path, std::move(environment), std::make_shared<OscEngineLogger>()),
      data_path_handler_(data_path_handler)
{
}

std::string OSCScenarioEngine::ResolveScenarioPath(const std::string& scenario_path) const
{
    return data_path_handler_.ResolvePathOrThrow(scenario_path);
}

std::string OSCScenarioEngine::ResolveMapPath(const std::string& map_path) const
{
    return data_path_handler_.ResolvePathOrThrow(map_path);
}

}  // namespace gtgen::simulator::osc_scenario_engine
