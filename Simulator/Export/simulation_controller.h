/*******************************************************************************
 * Copyright (c) 2020-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_SIMULATIONCONTROLLER_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_SIMULATIONCONTROLLER_H

#include <cstdint>
#include <functional>
#include <memory>

namespace gtgen
{

namespace core
{
namespace simulation
{
namespace simulation_controller
{
class SimulationControllerImpl;
}  // namespace simulation_controller
}  // namespace simulation
}  // namespace core

namespace simulator
{

/// @brief Allows to let the simulation execution be externally controlled, by launching an async server
/// that a client can connect to. Use as follows:
///     - create Controller (launches server)
///     - set SimulationStepCallback - gives the power of executing a sim step to the SimulationController
///     - set AbortCallback, to e.g. shut down the simulation cli when a client requests it
///     - call ExecuteStep() instead of stepping in the cli
///         - SimulationStepCallback will be executed unless a client paused the simulation. In this case the
///           SimulationController would wait with executing the callback until a client resumes the simulation
///           (or aborts it; in this case the step will be aborted as well)
///     - if a client aborted the simulation, the controller won't do anything anymore (so better set the callback)
class SimulationController
{
  public:
    explicit SimulationController(const std::string& console_log_level);
    SimulationController(const SimulationController&) = delete;
    SimulationController(SimulationController&& rhs) = delete;
    SimulationController& operator=(const SimulationController&) = delete;
    SimulationController& operator=(SimulationController&&) = delete;
    ~SimulationController();

    /// @brief Executes SimulationStepCallback depending on controller state (see class doxygen)
    void ExecuteStep();

    /// @param func Callback executed when a client requests to abort simulation
    void SetSimulationAbortCallback(std::function<void()> func);

    /// @param func Callback executed when ExecuteStep() is called OR a client requests a single step while
    /// simulation is paused
    void SetSimulationStepCallback(std::function<void()> func);

    /// @return The port on which the launched async server listens for simulation control clients
    std::uint16_t GetServerPort() const;

  private:
    std::unique_ptr<gtgen::core::simulation::simulation_controller::SimulationControllerImpl> impl_;
};

}  // namespace simulator
}  // namespace gtgen

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_SIMULATIONCONTROLLER_H
