/*******************************************************************************
 * Copyright (c) 2020-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_SIMULATOR_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_SIMULATOR_H

#include "i_host_vehicle_interface.h"
#include "simulation_parameters.h"

#include <chrono>
#include <memory>

namespace osi3
{
class SensorView;
class TrafficCommand;
class TrafficUpdate;
}  // namespace osi3

namespace gtgen::core::messages::ui
{
class DriverRelatedData;
}

namespace gtgen::simulator
{
class SimulatorImpl;

/// @brief GtGen Simulator class
class Simulator
{
  public:
    /// @brief Simulation constructor taking in the simulation parameters and the injected activity dependencies.
    /// Initialization of the logging is done here
    explicit Simulator(gtgen::core::SimulationParameters params);
    Simulator(const Simulator&) = delete;
    Simulator(Simulator&& rhs) = default;
    Simulator& operator=(const Simulator&) = delete;
    Simulator& operator=(Simulator&&) = delete;
    ~Simulator();

    /// @brief Initializes the simulation
    void Init();

    /// @brief Triggers the next step of the simulation
    void Step();

    /// @brief Returns true, when the scenario has ended
    bool IsFinished() const;

    /// @brief Shutdowns the simulation and cleans up
    void Shutdown();

    /// @brief Provides the configured step size after Simulator::Init() has been called
    std::chrono::milliseconds GetStepSize() const;

    /// @brief Provides the configured time scale after Simulator::Init() has been called
    double GetTimeScale() const;

    /// @brief Provides the scenario duration after Simulator::Init() has been called
    std::chrono::milliseconds GetScenarioDuration() const;

    /// @brief Provides proto interfaces for osi sensor view
    const osi3::SensorView& GetSensorView() const;

    /// @brief Provides proto interfaces for osi traffic command
    const std::vector<osi3::TrafficCommand>& GetTrafficCommands() const;

    void SetTrafficUpdate(osi3::TrafficUpdate traffic_update);

    /// @brief Provides driver takeover ability proto interfaces
    void SetDriverRelatedData(gtgen::core::messages::ui::DriverRelatedData driver_related_data);

    /// @brief Provides read access to internal host vehicle data
    /// @attention May only be called after Simulator::Init(), otherwise behavior is undefined
    const gtgen::core::IHostVehicleInterface& GetHostVehicleInterface() const;

    /// @deprecated This function will be removed in the coming releases.
    /// @brief Validates a passed in map. Only OpenDRIVE maps can be validated.
    ///
    /// @return Zero on success, error-code otherwise:
    ///          1 = Map contains warnings
    ///          2 = Map contains errors
    ///          3 = Unsupported map format
    [[deprecated]] int ValidateMap() const;

    /// @brief Validates a scenario given in simulation_parameters.
    ///
    /// @return Zero on success, error-code otherwise:
    ///          -1 = Unsupported scenario format to validate
    ///          >0 = Number of scenario errors
    static int ValidateScenario(const gtgen::core::SimulationParameters& simulation_parameters);

    /// @deprecated This function will be removed in the coming releases.
    /// @brief Installs GTGEN_DATA into the home directory
    [[deprecated]] void InstallUserData() const;

  private:
    std::unique_ptr<SimulatorImpl> impl_;
};

}  // namespace gtgen::simulator

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_EXPORT_SIMULATOR_H
