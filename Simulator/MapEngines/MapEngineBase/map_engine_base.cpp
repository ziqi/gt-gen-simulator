/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/MapEngineBase/map_engine_base.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Profiling/profiling.h"
#include "Simulator/Utils/Logging/logging.h"

namespace gtgen::simulator::environment::api
{
namespace fs = gtgen::core::fs;
using gtgen::core::environment::EnvironmentException;

map::MapConverterData MapEngineBase::ConvertData(const std::string& absolute_map_file_path,
                                                 const gtgen::core::service::user_settings::UserSettings& settings,
                                                 const mantle_api::MapDetails& map_details)
{

    auto converter_data = FillConverterData(settings, absolute_map_file_path, map_details);

    if (!fs::exists(converter_data.absolute_map_path))
    {
        throw EnvironmentException(
            "Cannot load a map. Map path '{}' does not exists. Please check the map path in the scenario file",
            converter_data.absolute_map_path);
    }

    Info("Creating GtGen map: {}", converter_data.absolute_map_path);
    GTGEN_PROFILE_MESSAGE(converter_data.absolute_map_path.c_str(), converter_data.absolute_map_path.size())

    return converter_data;
}

}  // namespace gtgen::simulator::environment::api
