/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/MapEngineOdr/map_engine_odr.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/filesystem.h"

#include <gtest/gtest.h>

namespace gtgen::simulator::environment::api
{
namespace fs = gtgen::core::fs;
namespace map = gtgen::core::environment::map;
using gtgen::core::environment::EnvironmentException;
using gtgen::core::environment::api::IMapEngine;

TEST(MapLoaderTest, GivenUnkownMap_WhenCreatingMap_ThenExceptionIsThrown)
{
    const fs::path odr_map_path{"StraightRoad2km.unkwn"};

    map::GtGenMap gtgen_map{};
    const gtgen::core::service::user_settings::UserSettings user_settings{};
    gtgen::core::service::utility::UniqueIdProvider unique_id_provider{};
    mantle_api::MapDetails map_details{};

    std::unique_ptr<IMapEngine> map_engine = std::make_unique<MapEngineOdr>();
    EXPECT_THROW(map_engine->Load(odr_map_path, user_settings, map_details, gtgen_map, &unique_id_provider),
                 EnvironmentException);
}

}  // namespace gtgen::simulator::environment::api
