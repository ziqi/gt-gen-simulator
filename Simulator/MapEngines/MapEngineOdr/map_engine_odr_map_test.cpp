/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/MapEngines/MapEngineOdr/map_engine_odr.h"

#include <MantleAPI/Common/position.h>
#include <gtest/gtest.h>

namespace gtgen::simulator::environment::api
{
namespace fs = gtgen::core::fs;
namespace map = gtgen::core::environment::map;
using units::literals::operator""_m;
using units::literals::operator""_deg;
using gtgen::core::environment::EnvironmentException;
using gtgen::core::environment::api::IMapEngine;

class MapLoaderTest : public testing::Test
{
  protected:
    const fs::path maps_path{"Simulator/Tests/Data/Maps"};
    const gtgen::core::service::user_settings::UserSettings user_settings{};
};

TEST_F(MapLoaderTest, GivenOdrMapFilepathAbsolute_WhenCreatingMap_ThenOdrMapIsCreated)
{
    const fs::path odr_map_path{maps_path / "Highway6kmStraight.xodr"};

    map::GtGenMap gtgen_map{};
    gtgen::core::service::utility::UniqueIdProvider unique_id_provider{};
    mantle_api::MapDetails map_details{};
    auto map_engine = std::make_unique<MapEngineOdr>();

    EXPECT_NO_THROW(map_engine->Load(odr_map_path, user_settings, map_details, gtgen_map, &unique_id_provider));
}

TEST_F(MapLoaderTest, GivenOdrMapFilepathRelativeToWorkingDirectory_WhenCreatingMap_ThenOdrMapIsCreated)
{
    const fs::path odr_map_path{maps_path / "Highway6kmStraight.xodr"};

    map::GtGenMap gtgen_map{};
    gtgen::core::service::utility::UniqueIdProvider unique_id_provider{};
    mantle_api::MapDetails map_details{};

    std::unique_ptr<IMapEngine> map_engine = std::make_unique<MapEngineOdr>();
    EXPECT_NO_THROW(map_engine->Load(odr_map_path, user_settings, map_details, gtgen_map, &unique_id_provider));
}

TEST_F(MapLoaderTest, GivenOdrMapWithoutPath_WhenCreatingMap_ThenExceptionIsThrown)
{
    std::string odr_map_path = fs::path("Highway6kmStraight.xodr");

    map::GtGenMap gtgen_map{};
    gtgen::core::service::utility::UniqueIdProvider unique_id_provider{};
    mantle_api::MapDetails map_details{};
    std::unique_ptr<IMapEngine> map_engine = std::make_unique<MapEngineOdr>();

    EXPECT_THROW(map_engine->Load(odr_map_path, user_settings, map_details, gtgen_map, &unique_id_provider),
                 EnvironmentException);
}

}  // namespace gtgen::simulator::environment::api
