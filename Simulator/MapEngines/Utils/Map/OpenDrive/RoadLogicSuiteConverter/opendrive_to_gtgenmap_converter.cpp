/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/Utils/Map/OpenDrive/RoadLogicSuiteConverter/opendrive_to_gtgenmap_converter.h"

#include "Core/Environment/Map/Common/map_converter_data.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/MapApiConverter/mapapi_to_gtgenmap_converter.h"
#include "Core/Service/Profiling/profiling.h"
#include "Simulator/MapEngines/Utils/Map/OpenDrive/RoadLogicSuiteConverter/Internal/opendrive_to_gtgenmap_coord_converter.h"
#include "Simulator/Utils/Logging/logging.h"

#include <RoadLogicSuite/map_converter_config.h>
#include <units.h>

namespace details
{
static void AddLaneGroup(std::unique_ptr<map_api::Map>& map_data)
{
    //  it's necessary to add one lane group to the map data because the current RLS does not handle lane-group
    // conversions. Lane groups are essential for the conversion to an ASTSA map. Therefore, we will create a default
    // lane group and include all lanes and lane boundaries in it.

    if (map_data->lane_groups.size() > 0)
    {
        return;
    }

    auto lane_group = std::make_unique<map_api::LaneGroup>();
    lane_group->id = 1;
    lane_group->type = map_api::LaneGroup::Type::kOther;

    for (auto& lane_boundary : map_data->lane_boundaries)
    {
        lane_group->lane_boundaries.push_back(*lane_boundary);
    }

    for (auto& lane : map_data->lanes)
    {
        lane_group->lanes.push_back(*lane);
    }

    map_data->lane_groups.push_back(std::move(lane_group));
}
}  // namespace details

namespace gtgen::simulator::environment::map::open_drive
{

void OpendriveToGtGenMapConverter::PreConvert()
{
    gtgen_map_.SetSourceMapType(GtGenMap::SourceMapType::kOpenDrive);
}

void OpendriveToGtGenMapConverter::ConvertInternal()
{
    GTGEN_PROFILE_SCOPE
    Info("Parsing OpenDrive map {}", data_.absolute_map_path);
    road_logic_suite::MapConverterConfig config;
    config.sampling_distance = units::length::meter_t{data_.lane_marking_distance_in_m},
    config.downsampling = data_.lane_marking_downsampling,
    config.downsampling_epsilon = units::length::meter_t{data_.lane_marking_downsampling_epsilon};

    road_logic_suite_ = std::make_shared<road_logic_suite::RoadLogicSuite>(config);
    auto map_data = road_logic_suite_->LoadMap(data_.absolute_map_path);

    // create default lane group
    details::AddLaneGroup(map_data);

    auto gtgen_map_converter = std::make_unique<MapApiToGtGenMapConverter>(unique_id_provider_, *map_data, gtgen_map_);
    gtgen_map_converter->Convert();

    gtgen_map_.coordinate_converter = std::make_unique<OpenDriveToGtGenMapCoordConverter>(road_logic_suite_);
}

void OpendriveToGtGenMapConverter::PostConvert()
{
    IGtGenMapConverterBase::PostConvert();
}

std::map<mantle_api::UniqueId, mantle_api::UniqueId> OpendriveToGtGenMapConverter::GetNativeToGtGenTrafficLightIdMap()
    const
{
    ///@todo not supported yet from road-logic-suite
    return std::map<mantle_api::UniqueId, mantle_api::UniqueId>{};
}

}  // namespace gtgen::simulator::environment::map::open_drive
