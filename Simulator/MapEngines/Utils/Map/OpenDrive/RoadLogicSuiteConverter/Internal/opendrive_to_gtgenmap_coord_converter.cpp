/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2025, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/Utils/Map/OpenDrive/RoadLogicSuiteConverter/Internal/opendrive_to_gtgenmap_coord_converter.h"

#include "Core/Environment/Exception/exception.h"
#include "Simulator/Utils/Logging/logging.h"

namespace gtgen::simulator::environment::map::open_drive
{
using gtgen::core::environment::EnvironmentException;

OpenDriveToGtGenMapCoordConverter::OpenDriveToGtGenMapCoordConverter(
    const std::shared_ptr<road_logic_suite::RoadLogicSuite>& road_logic_suite)
    : road_logic_suite_(road_logic_suite)
{
}

std::optional<mantle_api::Vec3<units::length::meter_t>> OpenDriveToGtGenMapCoordConverter::Convert(
    const UnderlyingMapCoordinate& coordinate) const
{
    if (!std::holds_alternative<mantle_api::OpenDriveLanePosition>(coordinate))
    {
        LogAndThrow(EnvironmentException("Tried to convert LatLong position with OpenDrive converter."));
    }
    const auto odr_position = std::get<mantle_api::OpenDriveLanePosition>(coordinate);
    return road_logic_suite_->ConvertLaneToInertialCoordinates(odr_position);
}

std::optional<UnderlyingMapCoordinate> OpenDriveToGtGenMapCoordConverter::Convert(
    const mantle_api::Vec3<units::length::meter_t>& coordinate) const
{
    return road_logic_suite_->ConvertInertialToLaneCoordinates(coordinate);
}

mantle_api::Orientation3<units::angle::radian_t> OpenDriveToGtGenMapCoordConverter::GetRoadOrientation(
    const mantle_api::OpenDriveRoadPosition& open_drive_road_position) const
{
    // @todo: Only supports 1D road orientation currently; extend to 3D when supported.
    const auto result =
        road_logic_suite_->GetRoadPositionHeading(open_drive_road_position.road, open_drive_road_position.s_offset);

    if (!result)
    {
        throw EnvironmentException(
            "Failed to retrieve road orientation for road ID: '{}' at offset: '{}' meters using the OpenDrive "
            "converter.",
            open_drive_road_position.road,
            open_drive_road_position.s_offset);
    }

    // Currently assuming no pitch or roll; modify for full 3D orientation support.
    return mantle_api::Orientation3<units::angle::radian_t>{
        *result, units::angle::radian_t(0.0), units::angle::radian_t(0.0)};
}

mantle_api::Orientation3<units::angle::radian_t> OpenDriveToGtGenMapCoordConverter::GetLaneOrientation(
    const mantle_api::OpenDriveLanePosition& open_drive_lane_position) const
{
    // @todo: Getting lane orientation using the OpenDrive converter is currently unsupported. Return road orientation
    // to ensure that the existing tests pass.
    const auto result =
        road_logic_suite_->GetRoadPositionHeading(open_drive_lane_position.road, open_drive_lane_position.s_offset);

    if (!result)
    {
        throw EnvironmentException(
            "Failed to retrieve lane orientation for road ID: '{}' at offset: '{}' meters using the OpenDrive "
            "converter.",
            open_drive_lane_position.road,
            open_drive_lane_position.s_offset);
    }

    return mantle_api::Orientation3<units::angle::radian_t>{
        *result, units::angle::radian_t(0.0), units::angle::radian_t(0.0)};
}
}  // namespace gtgen::simulator::environment::map::open_drive
