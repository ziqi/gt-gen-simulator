/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_MAPENGINEFACTORY_MAPENGINEFACTORY_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_MAPENGINEFACTORY_MAPENGINEFACTORY_H

#include "Core/Environment/GtGenEnvironment/Internal/i_map_engine.h"

#include <string>
namespace gtgen::simulator
{

class MapEngineFactory
{
  public:
    static std::unique_ptr<gtgen::core::environment::api::IMapEngine> Create(const std::string& map_path);
};
}  // namespace gtgen::simulator
#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_MAPENGINEFACTORY_MAPENGINEFACTORY_H
