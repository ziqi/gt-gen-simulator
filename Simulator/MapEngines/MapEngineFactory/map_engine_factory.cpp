/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/MapEngines/MapEngineFactory/map_engine_factory.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Simulator/MapEngines/MapEngineOdr/map_engine_odr.h"
namespace gtgen::simulator
{
using gtgen::core::environment::EnvironmentException;
using gtgen::core::environment::api::IMapEngine;

std::unique_ptr<IMapEngine> MapEngineFactory::Create(const std::string& map_path)
{
    if (gtgen::core::service::file_system::IsOdrMap(map_path))
    {
        return std::make_unique<environment::api::MapEngineOdr>();
    }
    else
    {
        throw EnvironmentException("Unsupported map file : '{}'. The map file must be '.xodr'.", map_path);
    }
}
}  // namespace gtgen::simulator
