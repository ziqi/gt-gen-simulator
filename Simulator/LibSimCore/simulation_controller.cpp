/*******************************************************************************
 * Copyright (c) 2020-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Export/simulation_controller.h"

#include "Core/Simulation/Simulator/simulation_controller_impl.h"

namespace gtgen::simulator
{

SimulationController::SimulationController(const std::string& console_log_level)
    : impl_{std::make_unique<gtgen::core::simulation::simulation_controller::SimulationControllerImpl>()}
{
    impl_->StartControlServer(console_log_level);
}

SimulationController::~SimulationController()
{
    impl_->ShutDown();
}

void SimulationController::ExecuteStep()
{
    impl_->ExecuteStep();
}

void SimulationController::SetSimulationAbortCallback(std::function<void()> func)
{
    impl_->SetSimulationAbortCallback(std::move(func));
}

void SimulationController::SetSimulationStepCallback(std::function<void()> func)
{
    impl_->SetSimulationStepCallback(std::move(func));
}

std::uint16_t SimulationController::GetServerPort() const
{
    return impl_->GetServerPort();
}

}  // namespace gtgen::simulator
