/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_LIBSIMCORE_SIMULATORIMPL_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_LIBSIMCORE_SIMULATORIMPL_H

#include "Core/Environment/GtGenEnvironment/gtgen_environment.h"
#include "Core/Export/i_host_vehicle_interface.h"
#include "Core/Export/simulation_parameters.h"
#include "Simulator/Utils/UserData/user_data_manager.h"
#include "driver_related_data.pb.h"
#include "osi_sensorview.pb.h"
#include "osi_trafficcommand.pb.h"
#include "osi_trafficupdate.pb.h"

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Execution/i_scenario_engine.h>

#include <chrono>
#include <memory>

namespace gtgen
{
namespace core
{
namespace simulation
{
namespace simulator
{
class SimulatorCore;
}  // namespace simulator
}  // namespace simulation
}  // namespace core

namespace simulator
{
namespace messages = gtgen::core::messages;
using gtgen::core::SimulationParameters;

class SimulatorImpl
{
  public:
    /// @brief Simulation constructor taking in the simulation parameters and the injected activity dependencies.
    /// Initialization of the logging is done here
    explicit SimulatorImpl(SimulationParameters params);
    SimulatorImpl(const SimulatorImpl&) = delete;
    SimulatorImpl(SimulatorImpl&& rhs) = default;
    SimulatorImpl& operator=(const SimulatorImpl&) = delete;
    SimulatorImpl& operator=(SimulatorImpl&&) = delete;
    ~SimulatorImpl();

    /// @brief Initializes the simulation
    void Init();

    /// @brief Triggers the next step of the simulation
    void Step();

    /// @brief Returns true, when the scenario has ended
    bool IsFinished() const;

    /// @brief Shutdowns the simulation and cleans up
    void Shutdown();

    /// @brief Provides the configured step size after SimulatorImpl::Init() has been called
    std::chrono::milliseconds GetStepSize() const;

    /// @brief Provides the configured time scale after SimulatorImpl::Init() has been called
    double GetTimeScale() const;

    /// @brief Provides the scenario duration after SimulatorImpl::Init() has been called
    std::chrono::milliseconds GetScenarioDuration() const;

    /// @brief Provides proto interfaces for osi sensor view
    const osi3::SensorView& GetSensorView() const;

    /// @brief Provides proto interfaces for osi traffic command
    const std::vector<osi3::TrafficCommand>& GetTrafficCommands() const;

    void SetTrafficUpdate(const osi3::TrafficUpdate& traffic_update);

    /// @brief Provides driver takeover ability proto interfaces
    void SetDriverRelatedData(messages::ui::DriverRelatedData driver_related_data);

    /// @brief Provides read access to internal host vehicle data
    /// @attention May only be called after SimulatorImpl::Init(), otherwise behavior is undefined
    const gtgen::core::IHostVehicleInterface& GetHostVehicleInterface() const;

    /// @brief Validates a scenario given in simulation_parameters.
    ///
    /// @return Zero on success, error-code otherwise:
    ///          -1 = Unsupported scenario format to validate
    ///          >0 = Number of scenario errors
    static int ValidateScenario(const gtgen::core::SimulationParameters& simulation_parameters);

    /// @brief Used by driver_related_data_test to connect test tcp client to the simulator's server
    std::uint16_t GetServerPort() const;

  private:
    std::unique_ptr<mantle_api::IScenarioEngine> CreateScenarioEngine() const;

    std::shared_ptr<simulation::user_data::UserDataManager> user_data_manager_;
    std::unique_ptr<gtgen::core::simulation::simulator::SimulatorCore> core_;
    std::shared_ptr<gtgen::core::environment::api::GtGenEnvironment> environment_;
    gtgen::core::SimulationParameters params_;
};

}  // namespace simulator
}  // namespace gtgen

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_LIBSIMCORE_SIMULATORIMPL_H
