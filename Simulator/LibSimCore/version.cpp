/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Simulator/Export/version.h"

#include "Simulator/LibSimCore/version.h"

namespace gtgen::simulator
{

std::string GetSimulatorVersion()
{
    return fmt::format("{}", gtgen_simulator_version);
}

}  // namespace gtgen::simulator
