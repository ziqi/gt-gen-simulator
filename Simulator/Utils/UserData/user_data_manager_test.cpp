/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Simulator/Utils/UserData/user_data_manager.h"

#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Simulation/Simulator/exception.h"
#include "Simulator/LibSimCore/version.h"

#include <gtest/gtest.h>

#include <fstream>

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;
using gtgen::core::simulation::simulator::SimulatorException;

TEST(UserDataManagerTest, GivenCustomGtGenDataPath_WhenIsGtGenDataInstallationNeeded_ThenInstallationNotNeeded)
{
    fs::path input_path{"anyhwere/gtgen_data"};
    UserDataManager user_data_manager(input_path, "");

    EXPECT_FALSE(user_data_manager.IsGtGenDataInstallationNeeded());
}

TEST(UserDataManagerTest,
     GivenEmptyCustomGtGenDataPathAndGtGenDataDoesNotExist_WhenIsGtGenDataInstallationNeeded_ThenInstallationNeeded)
{
    bool backup_created = false;
    fs::path gtgen_data_backup;
    auto default_gtgen_data = fs::current_path() / "GTGEN_DATA";
    if (fs::exists(default_gtgen_data))
    {
        // Backup the GTGEN_DATA so it does not exist for this test
        gtgen_data_backup = gtgen::core::service::file_system::DetermineBackupFileName(default_gtgen_data);
        fs::rename(default_gtgen_data, gtgen_data_backup);
        backup_created = true;
    }

    UserDataManager user_data_manager("", "", []() { return fs::current_path() / "GTGEN_DATA"; });

    EXPECT_TRUE(user_data_manager.IsGtGenDataInstallationNeeded());

    // Make sure we restore the original GTGEN_DATA in the home directory
    if (backup_created)
    {
        fs::rename(gtgen_data_backup, default_gtgen_data);
    }
}

TEST(UserDataManagerTest,
     GivenEmptyCustomGtGenDataPathAndGtGenDataOutdated_WhenIsGtGenDataInstallationNeeded_ThenInstallationNeeded)
{
    bool created = false;
    auto default_gtgen_data = fs::current_path() / "GTGEN_DATA";
    if (!fs::exists(default_gtgen_data))
    {
        // Just create an empty GTGEN_DATA folder, if no .gtgeninstallation version is assumed to be 0.0.0
        fs::create_directories(default_gtgen_data);
        created = true;
    }
    else if (fs::exists(default_gtgen_data / ".gtgeninstallation"))
    {
        fs::rename(default_gtgen_data / ".gtgeninstallation", default_gtgen_data / ".gtgeninstallation.bak");
    }

    UserDataManager user_data_manager("", "", []() { return fs::current_path() / "GTGEN_DATA"; });

    EXPECT_TRUE(user_data_manager.IsGtGenDataInstallationNeeded());

    if (created)
    {
        // If we created an empty GTGEN_DATA folder we need to remove it afterwards again
        fs::remove(default_gtgen_data);
    }
    else if (fs::exists(default_gtgen_data / ".gtgeninstallation.bak"))
    {
        fs::rename(default_gtgen_data / ".gtgeninstallation.bak", default_gtgen_data / ".gtgeninstallation");
    }
}

TEST(UserDataManagerTest, GivenNonExistingGtGenDataPath_WhenInitialize_ThenThrowException)
{
    fs::path non_existing_data_path("./Simulator/Tests/Data/MyCrazyDataStuffCollection/");
    fs::remove(non_existing_data_path);

    UserDataManager user_data_manager(non_existing_data_path, "");

    EXPECT_THROW(user_data_manager.Initialize(""), SimulatorException);
}

TEST(UserDataManagerTest, GivenExistingUserSettingsFile_WhenInitialize_ThenDoNotUpgradeOutdatedUserSettings)
{
    std::string expected_settings_content = "[HostVehicle]\nMovement = InternalVehicle";
    fs::path custom_gtgen_data{fs::current_path() / "custom-gtgen-data"};
    fs::path user_settings{custom_gtgen_data / "UserSettings" / "UserSettings.ini"};
    gtgen::core::service::file_system::CreateFile(user_settings, expected_settings_content);

    UserDataManager user_data_manager(custom_gtgen_data, "");
    user_data_manager.Initialize("");

    std::stringstream user_settings_content;
    {
        std::ifstream read_stream(user_settings.string());
        user_settings_content << read_stream.rdbuf();
    }

    EXPECT_EQ(expected_settings_content, user_settings_content.str());
}

TEST(UserDataManagerTest, GivenCustomGtGenDataWithoutUserSettings_WhenInitialize_ThenThrow)
{
    fs::path custom_gtgen_data_without_user_settings(fs::current_path() / "custom-no-user-settings");
    gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(custom_gtgen_data_without_user_settings,
                                                                          "UserSettings");
    UserDataManager user_data_manager(custom_gtgen_data_without_user_settings, "");

    EXPECT_THROW(user_data_manager.Initialize(""), SimulatorException);
}

TEST(UserDataManagerTest, GivenCustomGtGenDataWithSameVersion_WhenIsGtGenDataInstallationNeeded_ThenNoUpgrade)
{
    fs::path custom_gtgen_data{fs::current_path() / "custom_asts_data_up_to_date"};
    gtgen::core::service::file_system::CreateFile(custom_gtgen_data / ".gtgeninstallation",
                                                  fmt::format("{}", gtgen::simulator::gtgen_simulator_version));

    UserDataManager user_data_manager(custom_gtgen_data, "");

    EXPECT_FALSE(user_data_manager.IsGtGenDataInstallationNeeded());
}

}  // namespace gtgen::simulator::simulation::user_data
