/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Utils/UserData/user_data_manager.h"

#include "Core/Simulation/Simulator/exception.h"
#include "Simulator/LibSimCore/version.h"
#include "Simulator/Utils/UserData/Internal/file_helper.h"
#include "Simulator/Utils/UserData/Internal/path_helper.h"
#include "Simulator/Utils/UserData/Internal/user_data_installer.h"

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;
using gtgen::core::simulation::simulator::SimulatorException;

UserDataManager::UserDataManager(const fs::path& custom_gtgen_data,
                                 const fs::path& custom_user_settings,
                                 std::function<fs::path()> get_gtgen_data_directory,
                                 std::function<fs::path()> install_source_directory)
    : gtgen_data_directory_{custom_gtgen_data},
      user_settings_{custom_user_settings},
      get_gtgen_data_directory_{std::move(get_gtgen_data_directory)},
      install_source_directory_{std::move(install_source_directory)}
{
    if (custom_gtgen_data.empty())
    {
        gtgen_data_directory_ = get_gtgen_data_directory_();
    }

    data_path_handler_.SetGtGenDataDirectory(gtgen_data_directory_);

    if (custom_user_settings.empty())
    {
        user_settings_ = data_path_handler_.GetGtGenDataUserSettingsFile();
    }
}

bool UserDataManager::IsGtGenDataInstallationNeeded() const
{
    return IsDefaultGtGenDataUsed() &&
           (!fs::exists(gtgen_data_directory_) ||
            ReadVersionFile(gtgen_data_directory_) < gtgen::simulator::gtgen_simulator_version);
}

void UserDataManager::Install() const
{
    UserDataInstaller user_data_installer{gtgen::simulator::gtgen_simulator_version};
    user_data_installer.InstallOrUpgrade(gtgen_data_directory_, install_source_directory_(), user_settings_);
}

void UserDataManager::Initialize(const std::string& scenario_file)
{
    if (!fs::exists(gtgen_data_directory_))
    {
        throw SimulatorException(
            "Provided custom GTGen data directory '{}' does not exist! If you want to install it there, run: gtgen_cli "
            "-i -d {}",
            gtgen_data_directory_.string(),
            gtgen_data_directory_.string());
    }

    InitializeUserSettings();

    if (!scenario_file.empty() && fs::path(scenario_file).has_extension())
    {
        data_path_handler_.SetBasePath(data_path_handler_.ResolvePathOrThrow(scenario_file).parent_path());
    }
}

void UserDataManager::InitializeUserSettings()
{
    if (!fs::exists(user_settings_))
    {
        throw SimulatorException(
            "Provided user settings file '{}' does not exist! GTGen needs a valid user settings file in order to start "
            "simulation. Please make sure the provided path in the SimulationParameters struct is valid, or empty to "
            "use the settings from GTGEN_DATA.",
            user_settings_.string());
    }

    user_settings_handler_.Init(user_settings_);

    SetAdditionalDirectoriesFromUserSettings();
}

void UserDataManager::SetAdditionalDirectoriesFromUserSettings()
{
    user_settings_handler_.ReadFromFile();

    auto user_dirs = user_settings_handler_.GetUserSettings().user_directories;
    data_path_handler_.SetAdditionalScenarioDirectories(user_dirs.scenarios.elements);
    data_path_handler_.SetAdditionalMapDirectories(user_dirs.maps.elements);
}

const DataPathHandler& UserDataManager::GetPathHandler() const
{
    return data_path_handler_;
}

const gtgen::core::service::user_settings::UserSettings& UserDataManager::GetUserSettings() const
{
    return user_settings_handler_.GetUserSettings();
}

bool UserDataManager::IsDefaultGtGenDataUsed() const
{
    return gtgen_data_directory_ == get_gtgen_data_directory_();
}

}  // namespace gtgen::simulator::simulation::user_data
