/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Utils/UserData/data_path_handler.h"

#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Simulation/Simulator/exception.h"
#include "Simulator/Utils/Logging/logging.h"

#include <algorithm>
#include <map>

namespace gtgen::simulator::simulation::user_data
{

namespace
{

namespace fs = gtgen::core::fs;
using gtgen::core::simulation::simulator::SimulatorException;

class PathResolver
{
  public:
    PathResolver(const fs::path& input_path, const fs::path& base_path) : input_path_{input_path}, base_path_{base_path}
    {
    }

    void SetSearchDirectories(const std::vector<fs::path>& search_directories)
    {
        search_directories_ = search_directories;
    }

    void SetGtGenDataSubFolder(const fs::path& gtgen_data_subfolder) { gtgen_data_subfolder_ = gtgen_data_subfolder; }

    fs::path Resolve()
    {
        // Precedence:
        // 0. Replace potential '~' with home directory
        // 1. Absolute path
        // 2. Relative to working dir
        // 3. Relative to base path (main usage: map specified relative to scenario file)
        // 4. + 5. Recursive search within possible custom directories and GTGEN_DATA (only valid for map and scenario
        // files)

        gtgen::core::service::file_system::ReplaceTildeWithAbsoluteHomeDirectoryPath(input_path_);
        if (input_path_.is_absolute())
        {
            if (!fs::exists(input_path_))
            {
                LogAndThrow(SimulatorException("Input file '{}' does not exist.", input_path_.string()));
            }
            resolved_path_ = input_path_;
        }
        else if (!IsInWorkingDirectory() && !IsRelativeToBasePath() && !IsContainedInSearchDirectories() &&
                 !IsContainedInGtGenData())
        {
            throw SimulatorException(
                "Input file path '{}' could not be resolved. The following locations have been considered:\n{}",
                input_path_.string(),
                not_found_files_);
        }

        return fs::canonical(resolved_path_);
    }

  private:
    bool IsInWorkingDirectory()
    {
        fs::path working_directory_path = fs::current_path() / input_path_;
        if (fs::exists(working_directory_path))
        {
            resolved_path_ = working_directory_path;
            return true;
        }

        not_found_files_.append(working_directory_path.string() + "\n");
        return false;
    }

    bool IsRelativeToBasePath()
    {
        if (base_path_.empty())
        {
            return false;
        }

        fs::path relative_to_base_path = base_path_ / input_path_;
        if (fs::exists(relative_to_base_path))
        {
            resolved_path_ = relative_to_base_path;
            return true;
        }

        not_found_files_.append(relative_to_base_path.string() + "\n");
        return false;
    }

    bool IsContainedInSearchDirectories()
    {
        auto is_contained_in_directory = [this](const auto& search_directory) {
            auto found_path =
                gtgen::core::service::file_system::SearchRecursivelyInDirectory(input_path_, search_directory);
            if (!found_path.empty())
            {
                resolved_path_ = found_path;
                return true;
            }
            not_found_files_.append(search_directory.string() + " (including all sub-folders)\n");
            return false;
        };

        // Recursively search in the directories
        return std::any_of(search_directories_.cbegin(), search_directories_.cend(), is_contained_in_directory);
    }

    bool IsContainedInGtGenData()
    {
        if (gtgen_data_subfolder_.empty())
        {
            return false;
        }

        auto found_path =
            gtgen::core::service::file_system::SearchRecursivelyInDirectory(input_path_, gtgen_data_subfolder_);
        if (!found_path.empty())
        {
            resolved_path_ = found_path;
            return true;
        }
        not_found_files_.append(gtgen_data_subfolder_.string() + " (including all sub-folders)\n");
        return false;
    }

    fs::path input_path_;
    fs::path base_path_;
    fs::path resolved_path_{};
    std::string not_found_files_{};
    std::vector<fs::path> search_directories_{};
    fs::path gtgen_data_subfolder_{};
};

}  // namespace

void DataPathHandler::SetGtGenDataDirectory(std::string gtgen_data_directory)
{
    gtgen_data_directory_ = std::move(gtgen_data_directory);
    data_directory_set_ = true;
}

fs::path DataPathHandler::GetDefaultGtGenDataDirectory()
{
    return gtgen::core::service::file_system::GetHomeDirectory() / "GTGEN_DATA";
}

void DataPathHandler::SetBasePath(std::string base_path)
{
    base_path_ = std::move(base_path);
    ASSERT((base_path_.empty() || fs::exists(base_path_)) && "Provided base path does not exist.")
}

void DataPathHandler::SetAdditionalScenarioDirectories(const std::vector<std::string>& additional_directories)
{
    std::transform(additional_directories.begin(),
                   additional_directories.end(),
                   std::back_inserter(custom_scenario_directories_),
                   [](const std::string& directory) {
                       Info("Setting additional scenario directory: {}", directory);
                       return fs::path(directory);
                   });
}

void DataPathHandler::SetAdditionalMapDirectories(const std::vector<std::string>& additional_directories)
{
    std::transform(additional_directories.begin(),
                   additional_directories.end(),
                   std::back_inserter(custom_map_directories_),
                   [](const std::string& directory) {
                       Info("Setting additional map directory: {}", directory);
                       return fs::path(directory);
                   });
}

fs::path DataPathHandler::GetGtGenDataConfigDirectory() const
{
    return GetGtGenDataDirectory() / "UserSettings";
}

fs::path DataPathHandler::GetGtGenDataUserSettingsFile() const
{
    return GetGtGenDataConfigDirectory() / "UserSettings.ini";
}

fs::path DataPathHandler::GetGtGenDataLogDirectory() const
{
    return GetGtGenDataDirectory() / "Log";
}

fs::path DataPathHandler::GetGtGenDataOpenScenarioDirectory() const
{
    return GetGtGenDataDirectory() / "Scenarios/XOSC";
}

fs::path DataPathHandler::GetGtGenDataMapsDirectory() const
{
    return GetGtGenDataDirectory() / "Maps";
}

fs::path DataPathHandler::GetGtGenDataDirectory() const
{
    ASSERT(data_directory_set_ && "GTGEN_DATA directory path wasn't initialized!")

    return gtgen_data_directory_;
}

fs::path DataPathHandler::ResolvePathOrThrow(const fs::path& input_path, const fs::path& base_path) const
{
    ASSERT((base_path.empty() || fs::exists(base_path)) && "Provided base path does not exist.")

    PathResolver path_resolver(input_path, base_path.empty() ? base_path_ : base_path);

    if (gtgen::core::service::file_system::IsNdsMap(input_path) ||
        gtgen::core::service::file_system::IsOdrMap(input_path))
    {
        path_resolver.SetSearchDirectories(custom_map_directories_);
        path_resolver.SetGtGenDataSubFolder(GetGtGenDataMapsDirectory());
    }
    else if (gtgen::core::service::file_system::GetFileExtension(input_path) == "xosc")
    {
        path_resolver.SetSearchDirectories(custom_scenario_directories_);
        path_resolver.SetGtGenDataSubFolder(GetGtGenDataOpenScenarioDirectory());
    }

    return path_resolver.Resolve();
}

}  // namespace gtgen::simulator::simulation::user_data
