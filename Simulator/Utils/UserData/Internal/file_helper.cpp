/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Utils/UserData/Internal/file_helper.h"

#include "Simulator/Utils/Logging/logging.h"

#include <fstream>
#include <regex>

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;
using gtgen::core::Version;

Version ReadVersionFile(const fs::path& gtgen_data_directory)
{
    const auto version_file = gtgen_data_directory / ".gtgeninstallation";

    std::ifstream ifs(version_file);
    if (!ifs.is_open())
    {
        Warn("Could not open version file {}! This means GTGEN will try to upgrade the user data!",
             version_file.string());
        return Version{0, 0, 0};
    }

    std::string line;
    std::getline(ifs, line);

    return ExtractVersionFromString(line, version_file.string());
}

Version ExtractVersionFromString(const std::string& version_string, const std::string& version_file)
{
    const std::regex version_regex{R"reg(([0-9]{1,4})\.([0-9]+)\.([0-9]+))reg"};
    std::smatch version_match;

    if (!std::regex_match(version_string, version_match, version_regex))
    {
        Warn(
            "Wrong version format in version file {} (Given version is {})! This means, GTGEN will try to upgrade the "
            "user data!",
            version_file,
            version_string);
        return Version{0, 0, 0};
    }

    const auto version_convert = [](const std::string& s) -> std::int32_t { return atoi(s.c_str()); };

    return Version{version_convert(version_match[1].str()),
                   version_convert(version_match[2].str()),
                   version_convert(version_match[3].str())};
}

void CreateOrUpgradeVersionFile(const fs::path& gtgen_data_directory, const Version& version_to_install)
{
    const auto version_file = gtgen_data_directory / ".gtgeninstallation";

    // upgrade is as simple as delete and rewrite, so no special code needed :)
    fs::remove(version_file);  // throws on error, not if the file doesn't exist (we don't care)

    std::ofstream ofs(version_file);
    if (!ofs)
    {
        throw std::system_error(
            errno, std::system_category(), "Failed to create version file " + version_file.string());
    }

    ofs << fmt::format("{}", version_to_install) << std::endl;
    ofs.close();
}

}  // namespace gtgen::simulator::simulation::user_data
