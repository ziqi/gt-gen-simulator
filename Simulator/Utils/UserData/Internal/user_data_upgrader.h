/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_USERDATAUPGRADER_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_USERDATAUPGRADER_H

#include "Core/Service/FileSystem/filesystem.h"

#include <set>

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

class UserDataUpgrader
{
  public:
    /// @brief  Checks if a file is part of files_to_upgrade; if yes it compares user and original file,
    ///         And if they diverge it will suggest an upgrade
    ///
    /// @param gtgen_file_path File **from** where GTGEN_DATA are installed
    /// @param user_file_path User file that the original file is checked against
    /// @return True if the user file should be upgraded
    bool ShouldBeUpgraded(const fs::path& gtgen_file_path, const fs::path& user_file_path) const;

    /// @brief Upgrade currently means: rename user file to .bak and replace it with default GTGEN_DATA file
    /// @param user_file_path User file that is supposed to be upgraded
    void RenameFileWithBakExtension(const fs::path& user_file_path);

    /// @brief Returns the number of created backup files
    std::uint32_t GetNumberOfBackups() const;

  protected:
    /// @brief Compares the content of the two provided TEXT(!) files, ignoring any white space.
    /// The check is expensive, so keep the list of files to upgrade as small as possible.
    bool AreFilesDifferent(const fs::path& gtgen_file_path, const fs::path& user_file_path) const;

  private:
    std::string ParseFileToString(const std::string& path) const;

    std::uint32_t backup_file_count_{0};

    /// @brief Blacklist of files which shall not be upgraded
    const std::set<std::string> files_to_skip = {"UserSettings.ini"};
};

}  // namespace gtgen::simulator::simulation::user_data

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_USERDATAUPGRADER_H
