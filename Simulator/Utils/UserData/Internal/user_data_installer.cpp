/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Utils/UserData/Internal/user_data_installer.h"

#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Logging/scoped_duration_logger.h"
#include "Core/Service/UserSettings/user_settings_handler.h"
#include "Core/Service/Utility/version.h"
#include "Simulator/Utils/Logging/logging.h"
#include "Simulator/Utils/UserData/Internal/file_helper.h"
#include "Simulator/Utils/UserData/Internal/path_helper.h"

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;
using gtgen::core::Version;

UserDataInstaller::UserDataInstaller(const Version& version_to_be_installed)
    : version_to_install_{version_to_be_installed}
{
}

void UserDataInstaller::InstallOrUpgrade(const fs::path& installation_path,
                                         const fs::path& installation_data_source_path,
                                         const fs::path& user_settings)
{
    bool gtgen_data_exist = fs::exists(installation_path);
    if (gtgen_data_exist)
    {
        BackupLegacyGtGenData(installation_path);
    }

    Info("{} '{}' from '{}'",
         gtgen_data_exist ? "Upgrading" : "Installing",
         installation_path.string(),
         installation_data_source_path.string());

    {
        gtgen::core::service::logging::ScopedDurationLogger scoped_logger("Installing GTGEN_DATA");
        InstallDirectoryRecursive(installation_data_source_path, installation_path);
    }

    InstallUserSettings(user_settings);

    CreateOrUpgradeVersionFile(installation_path, version_to_install_);

    if (gtgen_data_exist)
    {
        Info("Finished upgrade of GTGEN_DATA: {} files installed, {} files upgraded.",
             installed_file_count_,
             user_data_upgrader_.GetNumberOfBackups());
    }
    else
    {
        Info("Finished installation of GTGEN_DATA.");
    }
}

void UserDataInstaller::BackupLegacyGtGenData(const fs::path& installation_path) const
{
    if (fs::exists(installation_path) && ReadVersionFile(installation_path) <= Version{2022, 3, 0})
    {
        Warn("GTGEN_DATA is incompatible with current version. A backup will be created of your current GTGEN_DATA.");
        auto backup = gtgen::core::service::file_system::DetermineBackupFileName(installation_path);
        fs::rename(installation_path, backup);
    }
}

void UserDataInstaller::InstallUserSettings(const fs::path& user_settings) const
{
    gtgen::core::service::user_settings::UserSettingsHandler user_settings_handler;
    user_settings_handler.Init(user_settings);

    if (!fs::exists(user_settings))
    {
        gtgen::core::service::file_system::CreateDirectoryIfNotExisting(user_settings.parent_path());
    }
    else
    {
        user_settings_handler.ReadFromFile();
    }

    user_settings_handler.WriteToFile(version_to_install_);
}

// NOLINTNEXTLINE(misc-no-recursion)
void UserDataInstaller::InstallDirectoryRecursive(const fs::path& from_directory, const fs::path& to_directory)
{
    fs::create_directory(to_directory);

    // Because experimental::filesystem neither provides fs::relative, nor fs::copy_if,
    // we cannot use the recursive iterator and also have to filter manually
    for (const auto& entry : fs::directory_iterator(from_directory))
    {
        const auto& from_entry_path = entry.path();
        if (IsTestDirectory(from_entry_path) || IsMetaFile(from_entry_path))
        {
            continue;
        }

        const auto to_entry_path = to_directory / from_entry_path.filename();
        if (fs::is_directory(from_entry_path))
        {
            InstallDirectoryRecursive(from_entry_path, to_entry_path);
        }
        else
        {
            InstallFile(from_entry_path, to_entry_path);
        }
    }
}

void UserDataInstaller::InstallFile(const fs::path& source_file_path, const fs::path& target_file_path)
{
    if (fs::exists(target_file_path) && user_data_upgrader_.ShouldBeUpgraded(source_file_path, target_file_path))
    {
        user_data_upgrader_.RenameFileWithBakExtension(target_file_path);
    }

    if (!fs::exists(target_file_path))
    {
        fs::copy(source_file_path, target_file_path);
        gtgen::core::service::file_system::SetRequiredReadWritePermissions(
            target_file_path);  // for files copied from /opt

        installed_file_count_++;
    }
}

}  // namespace gtgen::simulator::simulation::user_data
