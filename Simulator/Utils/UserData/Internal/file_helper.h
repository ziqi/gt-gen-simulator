/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_FILEHELPER_H
#define GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_FILEHELPER_H

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Utility/version.h"

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

/// @brief Extract the Version from the given string.
/// @param version_string String that contains the version
/// @param version_file Filename where version_string is read from, used for logging only
/// @return The parsed version of Version{0, 0, 0} in case of an invalid version_string
gtgen::core::Version ExtractVersionFromString(const std::string& version_string, const std::string& version_file);

/// @brief Creates a version file if none is existing or overwrites existing ones with the provided version
/// @param gtgen_data_directory User data that should be
/// @param version_to_install
void CreateOrUpgradeVersionFile(const fs::path& gtgen_data_directory, const gtgen::core::Version& version_to_install);

/// @brief Tries to read version file in the provided user data directory
/// @param gtgen_data_directory User data directory where .gtgeninstallation file will be searched
/// @return Version read from file or 0.0.0 if an error occurred
gtgen::core::Version ReadVersionFile(const fs::path& gtgen_data_directory);

}  // namespace gtgen::simulator::simulation::user_data

#endif  // GTGEN_SIMULATOR_ENVSIMULATOR_UTILS_USERDATA_INTERNAL_FILEHELPER_H
