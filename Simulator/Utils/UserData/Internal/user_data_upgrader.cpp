/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Simulator/Utils/UserData/Internal/user_data_upgrader.h"

#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Utility/string_utils.h"
#include "Core/Simulation/Simulator/exception.h"
#include "Simulator/Utils/Logging/logging.h"

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;
using gtgen::core::simulation::simulator::SimulatorException;

bool UserDataUpgrader::ShouldBeUpgraded(const fs::path& gtgen_file_path, const fs::path& user_file_path) const
{
    ASSERT(gtgen_file_path.filename() == user_file_path.filename() &&
           "Should not check differently named files for upgrade!")

    if (std::find(files_to_skip.begin(), files_to_skip.end(), gtgen_file_path.filename()) != files_to_skip.end())
    {
        return false;
    }

    return AreFilesDifferent(gtgen_file_path, user_file_path);
}

void UserDataUpgrader::RenameFileWithBakExtension(const fs::path& user_file_path)
{
    Warn("Backing up '{}' (with extensions .bak) and replace it by a newer version provided by GTGen",
         user_file_path.string());

    fs::rename(user_file_path, gtgen::core::service::file_system::DetermineBackupFileName(user_file_path));
    backup_file_count_++;
}

std::uint32_t UserDataUpgrader::GetNumberOfBackups() const
{
    return backup_file_count_;
}

bool UserDataUpgrader::AreFilesDifferent(const fs::path& gtgen_file_path, const fs::path& user_file_path) const
{
    std::string user_file = ParseFileToString(user_file_path.string());
    std::string new_file = ParseFileToString(gtgen_file_path.string());

    return user_file != new_file;
}

std::string UserDataUpgrader::ParseFileToString(const std::string& path) const
{
    // using POSIX api because it's the fastest according to
    // http://0x80.pl/notesen/2019-01-07-cpp-read-file.html

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg) - calling c-style vararg function acceptable
    int file_descriptor = open(path.c_str(), O_RDONLY);
    if (file_descriptor < 0)
    {
        // check http://man7.org/linux/man-pages/man2/open.2.html#ERRORS for error codes
        throw SimulatorException("Failed to open file: {}! Error code: {}", path, file_descriptor);
    }

    struct stat file_info
    {
    };
    fstat(file_descriptor, &file_info);

    std::string file_content;
    file_content.resize(static_cast<std::size_t>(file_info.st_size));

    const auto read_bytes = read(file_descriptor, file_content.data(), static_cast<std::size_t>(file_info.st_size));
    if (read_bytes == -1)
    {
        throw SimulatorException("Failed to read content of file: {}!", path);
    }

    close(file_descriptor);

    return file_content;
}

}  // namespace gtgen::simulator::simulation::user_data
