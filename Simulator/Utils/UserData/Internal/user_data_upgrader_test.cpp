/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Simulator/Utils/UserData/Internal/user_data_upgrader.h"

#include "Core/Service/FileSystem/file_system_utils.h"

#include <gtest/gtest.h>

namespace gtgen::simulator::simulation::user_data
{

namespace fs = gtgen::core::fs;

TEST(UserDataUpgraderTest, GivenModifiedFileInBlacklist_WhenShouldBeUpgraded_ThenReturnFalse)
{
    auto folder = gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(),
                                                                                        "UserDataUpgraderTest");

    fs::path gtgen_original = folder / "installation_source/UserSettings.ini";
    fs::path user_modified = folder / "GTGEN_DATA/UserSettings.ini";
    gtgen::core::service::file_system::CreateFile(gtgen_original, "dummy settings");
    gtgen::core::service::file_system::CreateFile(user_modified, "dummy settings");

    UserDataUpgrader user_data_upgrader;
    EXPECT_FALSE(user_data_upgrader.ShouldBeUpgraded(gtgen_original, user_modified));
}

TEST(UserDataUpgraderTest, GivenModifiedFile_WhenShouldBeUpgraded_ThenReturnTrue)
{
    auto folder = gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(),
                                                                                        "UserDataUpgraderTest");

    fs::path gtgen_original = folder / "installation_source/Bicycle.xosc";
    fs::path user_modified = folder / "GTGEN_DATA/Bicycle.xosc";
    gtgen::core::service::file_system::CreateFile(gtgen_original, "my-original-bicycle");
    gtgen::core::service::file_system::CreateFile(user_modified, "my-modified-bicycle");

    UserDataUpgrader user_data_upgrader;
    EXPECT_TRUE(user_data_upgrader.ShouldBeUpgraded(gtgen_original, user_modified));
}

TEST(UserDataUpgraderTest, GivenUnmodifiedFile_WhenShouldBeUpgraded_ThenReturnFalse)
{
    auto folder = gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(),
                                                                                        "UserDataUpgraderTest");

    fs::path gtgen_original = folder / "installation_source/Bicycle.xosc";
    fs::path user_original = folder / "GTGEN_DATA/Bicycle.xosc";
    gtgen::core::service::file_system::CreateFile(gtgen_original, "my-original-bicycle");
    gtgen::core::service::file_system::CreateFile(user_original, "my-original-bicycle");

    UserDataUpgrader user_data_upgrader;
    EXPECT_FALSE(user_data_upgrader.ShouldBeUpgraded(gtgen_original, user_original));
}

TEST(UserDataUpgraderTest, GivenFileToBackup_WhenRenameFileWithBakExtension_ThenFileRenamedWithBakExtension)
{
    auto folder = gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(),
                                                                                        "UserDataUpgraderTest");
    fs::path expected_backup_file = folder / "file_to_backup.xosc.bak";
    fs::path file_to_backup = folder / "file_to_backup.xosc";
    gtgen::core::service::file_system::CreateEmptyFile(file_to_backup);

    UserDataUpgrader user_data_upgrader;
    user_data_upgrader.RenameFileWithBakExtension(file_to_backup);

    EXPECT_TRUE(fs::exists(expected_backup_file));
    EXPECT_FALSE(fs::exists(file_to_backup));
}

TEST(UserDataUpgraderTest, GivenOneCreatedBackupFile_WhenGetNumberOfBackups_ThenReturnOne)
{
    auto folder = gtgen::core::service::file_system::CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(),
                                                                                        "UserDataUpgraderTest");
    fs::path file_to_backup = folder / "file_to_backup.xosc";
    gtgen::core::service::file_system::CreateEmptyFile(file_to_backup);

    UserDataUpgrader user_data_upgrader;
    user_data_upgrader.RenameFileWithBakExtension(file_to_backup);

    EXPECT_EQ(1, user_data_upgrader.GetNumberOfBackups());
}

}  // namespace gtgen::simulator::simulation::user_data
