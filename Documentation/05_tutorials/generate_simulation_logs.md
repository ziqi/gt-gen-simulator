## GT-Gen Generate Simulation Logs Tutorial

This tutorial explains how to use `gt-gen-simulator` to run scenarios in .xosc format and store the simulation logs into a `.csv` file. The status of each entity in the scenario, including `XPosition`, `YPosition`, `ZPosition`, `YawAngle`, `PitchAngle`, `RollAngle`, `Lane`, `AccelerationEgo`, `IndicatorState`, `VelocityEgo` will be recorded in the simulation logs at each timestamp. The supported status attributes can be expanded in the future.


### Overview

1. Install `gt-gen-simulator`: Follow the installation instructions provided [here](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/02_users_guide/getting_started.md?ref_type=heads).
2. Data Folder: By default, gt-gen-simulator creates a `/home/$USER/GTGEN_DATA/` folder to store assets. You can run the command under the installed `gtgen_cli` path to install a default data directory, user maps and scenarios can be placed in this folder after installation:  
    ```bash
    ./gtgen_cli -i
    ```
    You can also specify a custom data directory if needed by following command:  
    ```bash
    ./gtgen_cli -d Your/Data/Directory/Path
    ```

3. Configuration: Utilize the [template UserSettings file](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/ExampleData/UserSettings/UserSettings.ini?ref_type=heads) for configuration. A default UserSettings file is installed in the `GTGEN_DATA` folder and will be used by default if a custom configuration path is not specified with the `-u` option. You can directly modify this settings file to generate simulation logs.
4. Simulation Logs: You need to specify a custom path for storing simulation results under the `SimulationResults` section in the configuration file. For example:
    ```
    [SimulationResults]
    LogCyclics = true
    OutputDirectoryPath = /some/path/to/output
    ```
5. Run the Simulation: Execute the following command to start a simulation and output the results to a .csv file:
    ```bash
    ./gtgen_cli -u UserSettings/UserSettings.ini -s scenario/ALKS_Scenario_4.4_1_CutInNoCollision.xosc
    ```
    or if you are using the default UserSettings as explained in Step 3:  
    ```bash
    ./gtgen_cli -s scenario/ALKS_Scenario_4.4_1_CutInNoCollision.xosc
    ```


