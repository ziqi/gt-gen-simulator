## GT-Gen Creating Supplemenatry Sign in OpenScenario Files Tutorial

This tutorial explains how to create supplementary signs in `.xosc` scenario files to be used in simulation within gt-gen-simulator. Before starting, please ensure that gt-gen-simulator is installed. If not, refer to the installation instructions provided [here](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/02_users_guide/getting_started.md?ref_type=heads).


### Overview

1. Use the supplementary sign catalog, located at `GTGEN_DATA/Scenarios/XOSC/Catalogs/MiscObjects/TrafficSignCatalog.xosc` after installing, to create supplementary signs.

2. In the .xosc scenario file, under the `Entities` section, define a supplementary sign as follows:
    ```xml
    <Entities>
        <ScenarioObject name="SpeedLimit60">
        <CatalogReference catalogName="TrafficSignCatalog" entryName="traffic_sign">
            <ParameterAssignments>
            <ParameterAssignment parameterRef="type" value="274"/>
            <ParameterAssignment parameterRef="sub_type" value="56"/>
            </ParameterAssignments>
        </CatalogReference>
        </ScenarioObject>
        <ScenarioObject name="TrucksOnly">
        <CatalogReference catalogName="TrafficSignCatalog" entryName="supplementary_sign">
            <ParameterAssignments>
            <ParameterAssignment parameterRef="mounted_to" value="SpeedLimit60"/>
            <ParameterAssignment parameterRef="type" value="1010"/>
            <ParameterAssignment parameterRef="sub_type" value="51"/>
            </ParameterAssignments>
        </CatalogReference>
        </ScenarioObject>
    </Entities>
    ```
   In this example, `TrucksOnly` is defined as a supplementary sign, while `SpeedLimit60` is the main sign. Note that the `mounted_to` parameter must match the main sign's name. The order of main and supplementary signs in an `.xosc` file does not matter, and multiple supplementary signs can be assigned to one main sign:

    ```xml
    <ScenarioObject name="TrucksOnly">
        <CatalogReference catalogName="TrafficSignCatalog" entryName="supplementary_sign">
            <ParameterAssignments>
            <ParameterAssignment parameterRef="mounted_to" value="SpeedLimit60"/>
            <ParameterAssignment parameterRef="type" value="1010"/>
            <ParameterAssignment parameterRef="sub_type" value="51"/>
            </ParameterAssignments>
        </CatalogReference>
        </ScenarioObject>
        <ScenarioObject name="AccidentHazard">
        <CatalogReference catalogName="TrafficSignCatalog" entryName="supplementary_sign">
            <ParameterAssignments>
            <ParameterAssignment parameterRef="mounted_to" value="SpeedLimit60"/>
            <ParameterAssignment parameterRef="type" value="1006"/>
            <ParameterAssignment parameterRef="sub_type" value="31"/>
            </ParameterAssignments>
        </CatalogReference>
        </ScenarioObject>
        <ScenarioObject name="SpeedLimit60">
            ...
        </ScenarioObject>
    ```


3. To initialize the position of a main sign, use `TeleportAction` as follows:
    ```xml
    <Init>
        <Actions>
            <Private entityRef="SpeedLimit60">
            <PrivateAction>
                <TeleportAction>
                <Position>
                    <LanePosition roadId="1" laneId="-1" offset="0.0" s="48.0">
                    <Orientation h="3.1415" type="relative"/>
                    </LanePosition>
                </Position>
                </TeleportAction>
            </PrivateAction>
            </Private>
        </Actions>
    </Init>
    ```
    **Note:** Initializing the position of a supplementary sign is optional. If no teleport action is assigned to a supplementary sign, its position will be automatically deduced based on its geometric center, vertical offset, and the properties of its main sign. 


4. After defining a scenario file correctly, you can start the simulation with the following command:
    ```bash
    ./gtgen_cli -s scenario/your_supplementary_sign_scenario.xosc
    ```
