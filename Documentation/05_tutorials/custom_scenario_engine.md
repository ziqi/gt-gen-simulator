##	Integrating Custom Scenario and Map Engines into GT-Gen Simulator

GT-Gen Simulator's design allows for the seamless integration of both scenario and map engines, enhancing its versatility and adaptability to various simulation needs.

### Core Components Overview

![gt_gen_architecture_engines](../figures/gt_gen_architecture_engines.svg)


**Scenario Engine**: Interprets scenario definitions, managing traffic participant movements, traffic light states, and evaluating conditions that trigger specific behaviors within the simulation. The default engine used is [OpenScenarioEngine](https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine)..

**Map Engine**: Handles parsing of map files, transforming them into a format usable by the simulator. The [RoadLogicSuite](https://gitlab.eclipse.org/eclipse/openpass/road-logic-suite) serves as the standard map engine.

**Environment**: Acts as a central hub for critical simulation data such as time, weather conditions, and traffic information, ensuring a cohesive simulation environment. See [GT-Gen-Core](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core)

These components are crucial for the comprehensive simulation capabilities of GT-Gen Simulator.




### Extending GT-Gen-Simulator with Custom Engines

GT-Gen supports the integration of alternative map and scenario engines, providing users the ability to tailor the simulator to their specific requirements. This is facilitated through the implementation of `IMapEngine` and `IScenarioEngine` interfaces, available within [MantleAPI](https://gitlab.eclipse.org/eclipse/openpass/mantle-api) for scenario engines, and [MapSDK](https://gitlab.eclipse.org/eclipse/openpass/map-sdk) for map engines.




### Custom Scenario Engine Integration

Integrating a custom scenario engine involves conforming to the `IScenarioEngine` interface, ensuring the custom engine can communicate effectively with GT-Gen Simulator. This process entails:


The integration process of a scenario engine can be seen as a 3-step process as follows:
1.	**Implement the IScenarioEngine Interface**: Implement all necessary methods as defined by the interface.
2.	**Modifying GT-Gen Simulator**: Update the simulator to recognize and instantiate the custom scenario engine.
3.	**Project Recompilation**: Rebuild the simulator project to apply the changes.

#### Considerations

To ensure a smooth integration of a custom scenario engine into GT-Gen Simulator, adhere to the following guidelines:

1. Initialization: Implement all initialization processes within the `Init()` method. This includes reading and validating the scenario file. It's advisable to encapsulate validation logic within a `ValidateScenario()` method, invoked from `Init()`.
2. Scenario Information: Utilize the `GetScenarioInfo()` method for the simulator to fetch parsed scenario details. It's crucial to comprehensively populate the returned structure, including the map's path within the `full_map_path` attribute of `additional_information`.
3. Simulation Steps: The `Step()` method, called repeatedly during simulation, is where you'll define the scenario's progression logic. This includes actions like moving traffic, altering traffic light states, integrating updates from external controllers, and managing the scenario's internal timing.
4. Scenario State: The simulator's `IsFinished()` method assesses if the scenario has concluded. Monitoring and accurately reporting the scenario's state through this method is essential.

#### Practical Example for Integration

1. Place the engine code in the `Simulator/ScenarioEngines/` directory to ensure it meshes well with GT-Gen's architecture.
2. If the engine depends on external libraries, add those to the `third_party/` directory and reference them in the `WORKSPACE` file for proper linkage.
3. Modify the `Simulator/ScenarioEngineFactory/scenario_engine_factory.cpp` file to create instances of your custom engine based on specific criteria, such as file extension or scenario attributes.

The following snippet for integrating a scenario engine compatible with `.xyz` scenario files:


```c++
// Example code:
std::unique_ptr<mantle_api::IScenarioEngine> ScenarioEngineFactory::Create(
    const std::string& file,
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    simulation::user_data::UserDataManager* user_data_manager)
{
//...
    if (file_path.extension() == ".xyz")
    {
        return std::make_unique<XyzScenarioEngine>(file, user_data_manager, environment);
    }
//...
}
```



### Custom Map Engine Integration

Similarly, integrating a custom map engine requires compliance with the `IMapEngine` interface. This straightforward process involves:

1. **Implementing the IMapEngine Interface**: Specifically, the Load method must be implemented to handle map loading based on the simulator's requirements.
2. **Simulator Modification**: Adjust the simulator's constructor to incorporate the custom map engine.
3. **Recompiling the Project**: Apply integration changes by rebuilding the simulator.


The `Load` method of the `IMapEngine` interface is critical for ensuring that the custom map engine can process map files and provide GT-Gen Simulator with a usable map object.


### Practical Example for Integration


Implement the customer map engine into the `Simulator/MapEngines/` directory, ensuring compatible.
If any dependencies are required, you can add them to the `third_party/` directory and  make sure to include an entry in the `WORKSPACE`.
Inject the map engine into the engine factory: `Simulator/MapEngineFactory/map_engine_factory.cpp`.

The following code demonstrates how to integrate a customer map engine that supports `xyz` map file:

```c++
// Example code:
std::unique_ptr<environment::api::IMapEngine> MapEngineFactory::Create(const std::string& map_path)
{
//...
    if (file_path.extension() == ".xyz")
    {
        return std::make_unique<XyzMapEngine>(...);
    }
//...
}
```

### Practical Integration Steps

For both scenario and map engines, the integration process includes placing the custom engine within the simulator's directory structure and ensuring all dependencies are appropriately managed. Specific file paths and dependencies should be added to the project's workspace for seamless integration.
