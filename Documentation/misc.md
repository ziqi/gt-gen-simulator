## Contributions
Brief info on how to contribute is found here: https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/CONTRIBUTING.md




## License
Eclipse Public License - v 2.0 : https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/LICENSE
