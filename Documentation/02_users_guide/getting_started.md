## Getting Started

You can run the example simulation in two ways: using prebuilt releases or by building and running from the source code. Follow the steps below based on your preference.

<br>

### Option 1: Using Docker-based demo pipeline
A Docker-based demo pipeline is provided to help users try out the simulator without installation.

  **1. Install Docker**

  See [Docker](https://www.docker.com/).

  **2. Pull prebuilt Docker**

  The demo pipeline is provided as a public Docker image in the [Github](https://hub.docker.com/repository/docker/avxautonomy/gt-gen-simulator/general).
  ```bash
  docker pull avxautonomy/gt-gen-simulator:latest
  ```

  Check the simulator version:
  ```bash
  docker run --rm  avxautonomy/gt-gen-simulator:latest -v
  ```

  You should see similar output as :
  ```bash
  Cli: 1.1.0
  GtGen-Core: 14.1.0
  GtGen-Simulator: 8.0.0
  ```

  **3. Run simulation**

  You can now mount your local directory containing the scenario and assets into the Docker container and run the simulation using the following command:

  ``` bash
  docker run --rm \
    -v $(pwd):/home/gt-gen-simulator \
    -v /tmp:/tmp \
    avxautonomy/gt-gen-simulator:latest \
    -s /home/gt-gen-simulator/ExampleData/Scenarios/XOSC/example_simple_cut_in.xosc \
    -u /home/gt-gen-simulator/ExampleData/UserSettings/UserSettings.ini
  ```

  Description of Parameters:

  - `-v $(pwd):/home/gt-gen-simulator`: Mounts your current directory into the container's `/home/gt-gen-simulator` directory.
  - `-v /tmp:/tmp`: Mounts the `/tmp` directory for storing simulation logs.
  - `-s`: Specifies the path to the scenario file (`example_simple_cut_in.xosc`).
  - `-u`: Specifies the path to the user settings file (`UserSettings.ini`).

  **Output:** Simulation logs are saved in the `/tmp` directory. The specific location of the logs is defined in the `UserSettings.ini` file. For more details on interpreting the simulation results, refer to the final section, **Verifying the Simulation Results**, in this document.


<hr>
<br>

### Option 2: Using Prebuilt Releases

  **1. Download the prebuilt release and installation**

  For an easy setup, download the `.deb` packages from our project's [release page](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/releases). These packages are designed for quick installation on Ubuntu systems. Open a terminal and navigate to the folder where you've downloaded the packages. To install, use the dpkg command:

  ```bash
  # Install GtGen-Simulator
  dpkg -i gtgen-simulator_{version}_amd64.deb
  # Install the command line tool:  GtGen-Cli
  dpkg -i gtgen-cli_{version}_amd64.deb
  ```

  After installation, the simulator library (`gtgen_core`) is located in `/opt/gtgen_core`, and the command-line tool (`gtgen_cli`) can be found at `/opt/gtgen_cli/gtgen_cli`.

  _(Optional)_ To make the tool accessible by calling `gtgen_cli` directly, you can create a symbolic link to the executable in a directory that's already included in your system's `PATH`, such as `/usr/local/bin`.

  ```bash
  sudo ln -s /opt/gtgen_cli/gtgen_cli /usr/local/bin/gtgen_cli
  ```


  **2. Run simulation with CLI-tool**

  With GT-Gen-Simulator and CLI installed, you're ready to launch simulations directly from the command line. For instance, to run a `.xosc` example scenario:

  ```bash
  gtgen_cli -s ExampleData/Scenarios/XOSC/example_simple_cut_in.xosc
  ```

  > 📖 Find more CLI Options in the section [GT-Gen Command-Line Interface (CLI) Overview](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/02_users_guide/cli.md?ref_type=heads)

<hr>
<br>

### Option 3: Building and Running from Source Code

  **1. Clone the Repository and install Prerequisites**

  Refer to the required prerequisites listed in this [Dockerfile](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/utils/Dockerfile?ref_type=heads). Make sure these dependencies are installed before proceeding.


  **2.Build and run the example scenario**

  ```bash
  bazel  run --config=gt_gen //Cli:gtgen_cli -- -s ExampleData/Scenarios/XOSC/example_simple_cut_in.xosc -u ExampleData/UserSettings/UserSettings.ini
  ```

<br>

## Verifying the Simulation Results
You can verify the simulation results using one or more of the following methods:

  **1. Simulation Logs**

  If the `SimulationResults/LogCyclics` option is enabled in your `UserSettings.ini` file, a cyclically recorded `CSV` file named `Cyclics_Run_xxx.csv` will be generated in the specified location. See section [Simulation Configuration](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/02_users_guide/sim_config.md?ref_type=heads) and [Generate Simulation Logs Tutorial](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/05_tutorials/generate_simulation_logs.md?ref_type=heads)

  **2. Trace Recording**

  If `TraceRecording/Enabled` option is set to `true` in your `UserSettings.ini` file, the simulation will generate an `OSI-format` trace-file. The file is named similarly to: `SensorView_Scenario_tpm_example_OSI_3_7_0_GTGEN_CORE_14_1_0.osi`, and can be found in the specified directory. See section [Simulation Configuration](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/02_users_guide/sim_config.md?ref_type=heads) and [GT-Gen Generate Trace Files Tutorial](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/05_tutorials/generate_trace_files.md?ref_type=heads)

  **3. 3D Live Streaming with LichtBlick**

  You can visualize the simulation in real time using **LichtBlick**. See the [Visualization Guide](https://gitlab.eclipse.org/eclipse/openpass/gt-gen-simulator/-/blob/main/Documentation/02_users_guide/lichtblick.md?ref_type=heads) for detailed instructions on setting up live streaming.
