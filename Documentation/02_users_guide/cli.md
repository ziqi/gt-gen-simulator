
## GT-Gen Command-Line Interface (CLI) Overview

The GT-Gen Simulator extends its functionality through a Command-Line Interface (CLI) tool, designed to facilitate both standalone simulations and task automation.


### Utilizing the CLI Tool

To execute commands using the GT-Gen CLI, follow the syntax pattern below:

```bash
gtgen_cli [command] [options] <parameter>
```

- `gtgen_cli`: The executable name for the CLI tool.
- `[command]`: Specifies the action you wish to perform, such as running a simulation or validating a map. (Note: Confirm the list of supported commands.)
- `[options] <parameter>`: These are additional inputs to refine or customize your command's execution.


### Options Overview

The CLI supports various options for tailored simulation control:

- **General Options**: Include help (`-h, --help`), version information (`-v, --version`), and setting the simulation data directory (`-d, --data`).
- **Simulation Execution**: Specify scenarios (`-s, --scenario`), custom user settings file (`-u, --user-settings-file`), and log file directory (`-l, --log-file-dir`).
- **Validation and Testing**: Validate map files (`-m, --validate-map`) and scenarios (`-j, --validate-scenario`) for integrity before execution.

Use the `gtgen_cli -h` to print more details of supported options.

### Executing Simulations

Run simulations directly from your command line by specifying the scenario, log file location, custom user settings, and simulation step size:

```bash
gtgen_cli run --scenario <scenario_path> --log-file-dir <logs_path> --user-settings-file <config_file> --step-size-ms 40
```
Replace `<scenario_path>`, `<logs_path>`, and `<config_file>` with your specific file paths.




###  Map and Scenario Validation

```bash
gtgen_cli --validate-map <map_path>
gtgen_cli --validate-scenario --scenario <scenario_path>
```

The CLI tool returns status codes to indicate the validation outcome: 0 for **success**, 1 for **warnings**, and 2 for **errors or exceptions**.
