##  Visualization

GT-Gen enhances simulation analysis by supporting run-time visualization of simulation outputs (ASAM-OSI message) via a Foxglove-WebSocket connection. This feature can work seamlessly with [Foxglove](https://foxglove.dev/) and [Lichtblick](https://github.com/Lichtblick-Suite/lichtblick), offering an intuitive way to observe and analyze simulation data dynamically.  


This documentation uses Lichtblick as a reference example to demonstrate data visualization. Lichtblick is an integrated visualization and diagnosis tool for robotics, available in your browser or as a desktop app on Linux, Windows, and macOS. To visualize data following the ASAM Open Simulation Interface (ASAM OSI) streaming standard, an additional converter extension that utilizes Lichtblick’s native 3D panel is also required.


To utilize WebSocket streaming, specific settings need to be adjusted within the **UserSettings.ini** file. You can specify a following section in the configuration file for visualization (Remark: The visualization is supported via Foxglove WebSocket protocol in Lichtblick, so the setting name is `Foxglove`).  
```
[Foxglove]
WebsocketServer = true
```   
Detailed instructions on configuring can be also found in the  [Simulator Configuration](sim_config.md) section.

### Install Lichtblick
Installation and configuration of Lichtblick are prerequisites for visualization and are not included in the GT-Gen package.
You can find the repository at [Lichtblick on GitHub](https://github.com/Lichtblick-Suite/lichtblick).

### Install 3D panel plugin
For installation of the converter extension that utilizes Lichtblick’s native 3D panel you can referr to the provided link [OSI-Converter Extension](https://github.com/Lichtblick-Suite/asam-osi-converter?tab=readme-ov-file). Once downloaded, install the extension in Lichtblick by dragging the .foxe file into the Lichtblick window.

### Open websocket connection in Lichtblick
To visualize GT-Gen's simulation output in Lichtblick:

1. Start Lichtblick and select Open a new connection.
2. Choose Foxglove WebSocket.
3. Start a simulation using GT-Gen-Simulator.
4. GT-Gen will then stream simulation data to Lichtblick's **default WebSocket address**, enabling real-time visualization.

<img src="../figures/lichtblick_setup.png" alt="lichtblick_setup" width="500" height="300">
