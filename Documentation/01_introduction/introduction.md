## GT-Gen Simulator

GT-Gen, short for Ground Truth Generator, is a simulation tool for enhancing the development and testing of Advanced Driver-Assistance Systems (ADAS) and Autonomous Driving (AD) technologies. It facilitates the testing and validation of autonomous functionalities within a controlled, risk-free virtual environment. The simulator generates a Ground-Truth using ASAM's Open Simulation Interface (OSI) standards, based on specific scenarios and maps defined by ASAM OpenSCENARIO and ASAM OpenDRIVE, respectively.

GT-Gen can be employed in two ways: as a standalone application designed to function alongside a Command-Line Interface tool (CLI), and, as a shared library that can be integrated into custom simulation applications.
