load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

# Note: please update the version in the patch file "update_version.patch" if the version changes
_TAG = "v17.0.2"

def gt_gen_core():
    maybe(
        http_archive,
        name = "gt_gen_core",
        url = "https://gitlab.eclipse.org/eclipse/openpass/gt-gen-core/-/archive/{tag}/gt-gen-core-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "05742721eab0d689104595116a8ffbe2d97ae0f1c0942c8ee639f303ab2504c1",
        strip_prefix = "gt-gen-core-{tag}".format(tag = _TAG),
        patches = [
            "@//Core:update_version.patch",
            "@//Core:revert_osi_version.proto_patching.patch",
        ],
        patch_args = ["-p1"],
    )
